//CustomLight.hlsl
#ifndef CUSTOM_LIGHTING_INCLUDED
#define CUSTOM_LIGHTING_INCLUDED

void CustomLight_float(float3 worldPos, out float3 direction, out float3 color, out float shadowAtten)
{
#if SHADERGRAPH_PREVIEW
		direction = half3(0, 1, 0);
		color = half3(0.5,0.5,0);
		shadowAtten = 1;
#else
#if SHADOWS_SCREEN
		float4 clipPos = TransformWorldToHClip(worldPos);
		float4 shadowCoord = ComputeScreenPos(clipPos);
#else
    float4 shadowCoord = TransformWorldToShadowCoord(worldPos);
#endif
    Light mainLight = GetMainLight();
    direction = mainLight.direction;
    color = mainLight.color;
    ShadowSamplingData shadowSamplingData = GetMainLightShadowSamplingData();
    float shadowStrength = GetMainLightShadowStrength();
    shadowAtten = SampleShadowmap(shadowCoord, TEXTURE2D_ARGS(_MainLightShadowmapTexture, sampler_MainLightShadowmapTexture), shadowSamplingData, shadowStrength, false);
#endif
#endif
}

void CustomLight_half(float3 worldPos, out half3 direction, out half3 color, out float shadowAtten)
{
	#if SHADERGRAPH_PREVIEW
		direction = half3(0, 1, 0);
		color = half3(0.5,0.5,0);
		shadowAtten = 1;
	#else
	#if SHADOWS_SCREEN
		float4 clipPos = TransformWorldToHClip(worldPos);
		float4 shadowCoord = ComputeScreenPos(clipPos);
	#else
		float4 shadowCoord = TransformWorldToShadowCoord(worldPos);
	#endif
		Light mainLight = GetMainLight();
		direction = mainLight.direction;
		color = mainLight.color;
		ShadowSamplingData shadowSamplingData = GetMainLightShadowSamplingData();
		float shadowStrength = GetMainLightShadowStrength();
		shadowAtten = SampleShadowmap(shadowCoord, TEXTURE2D_ARGS(_MainLightShadowmapTexture, sampler_MainLightShadowmapTexture), shadowSamplingData, shadowStrength, false); 
	#endif
}