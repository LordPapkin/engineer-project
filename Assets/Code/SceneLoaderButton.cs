using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneLoaderButton : MonoBehaviour
{
    [SerializeField] private string sceneToLoad;
    [SerializeField] private bool loadAdditive;

    Button button;

    void Awake()
    {
        button = GetComponent<Button>();
        button.onClick.AddListener(Load);
    }

    void OnDestroy()
    {
        button.onClick.RemoveAllListeners();
    }

    void Load()
    {
        string sceneToLoadFinal = sceneToLoad + "B";
        SceneManager.LoadScene(sceneToLoadFinal, loadAdditive ? LoadSceneMode.Additive : LoadSceneMode.Single);
    }
}
