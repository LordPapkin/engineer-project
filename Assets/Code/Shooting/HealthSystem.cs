using System;
using UnityEngine;

public class HealthSystem 
{
   public Action onHit;
   public Action onDeath;
   public Action onHeal;
   
   private int currentHealth;
   private int maxHealth;
   private StrengthsAndWeaknessesSO strengthsAndWeaknesses;
   
   public void TakeDamage(int damageAmount, ElementType attackElement)
   {
      int damageToDeal = 0;

      if (attackElement == ElementType.Normal || strengthsAndWeaknesses.EntityElementType == ElementType.Normal)
      {
         damageToDeal = damageAmount;
      }
      else if (attackElement == strengthsAndWeaknesses.ResistElementType)
      {
         damageToDeal = Mathf.FloorToInt(damageAmount * strengthsAndWeaknesses.ResistMultiplier);
      }
      else if (attackElement == strengthsAndWeaknesses.CounterElementType)
      {
         damageToDeal = Mathf.FloorToInt(damageAmount * strengthsAndWeaknesses.CounterMultiplier);
      }
      else
      {
         damageToDeal = damageAmount;
      }

      currentHealth -= damageToDeal;

      if(currentHealth <= 0)
         onDeath?.Invoke();
      else
         onHit?.Invoke();
   }

   public void Heal(int healAmount)
   {
      if(currentHealth == maxHealth)
         return;
      
      currentHealth += healAmount;
      currentHealth = Math.Clamp(currentHealth, 0, maxHealth);
      onHeal?.Invoke();
   }

   public void SetHealthSystem(int givenMaxHealth, StrengthsAndWeaknessesSO givenStrengthsAndWeaknesses)
   {
      maxHealth = givenMaxHealth;
      currentHealth = maxHealth;
      strengthsAndWeaknesses = givenStrengthsAndWeaknesses;
   }

   public float GetHealthRatio()
   {
      return (float)currentHealth / maxHealth;
   }
}
