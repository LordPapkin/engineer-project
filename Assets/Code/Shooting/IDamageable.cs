using System.Collections.Generic;
using System.Threading.Tasks;

public interface IDamageable
{
    void TakeDamage(ElementType elementType, int amount);
    void TakeEffect(Effect effect);
    void TakeEffect(Effect effect, Particles particleType);

    List<Effect> CurrentEffects { get; set; }
}

