public class ShutdownEffect : Effect
{
    public ShutdownEffect(float duration)
    {
        this.duration = duration;
        type = EffectType.Shutdown;
    }

    public override void Run() { }

    public override void Start()
    {
        affectedUnit.IsShutdown = true;
    }

    public override void End()
    {
        affectedUnit.IsShutdown = false;
    }
}
