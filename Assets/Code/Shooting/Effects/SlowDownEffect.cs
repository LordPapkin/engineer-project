using UnityEngine;

public class SlowDownEffect : Effect
{
    private Slime slime;
    
    public SlowDownEffect(float duration)
    {
        this.duration = duration;
        type = EffectType.SpeedUp;
    }

    public override void Run() { }

    public override void Start()
    {
        slime = affectedUnit as Slime;
        if (slime != null) 
            slime.ChangeSpeed(slime.SlimeData.MovementSpeed * 0.5f);
    }

    public override void End()
    {
        if (slime != null) 
            slime.ChangeSpeed(slime.SlimeData.MovementSpeed);
    }
}
