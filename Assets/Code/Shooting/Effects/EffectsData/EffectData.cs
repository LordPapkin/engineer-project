using UnityEngine;

[CreateAssetMenu(fileName = "New Effect Data", menuName = "ScriptableObjects/Combat/Effect Data")]
public class EffectData : ScriptableObject
{
    [field: SerializeField] public int EffectValue { get; private set; }
    [field: SerializeField] public float EffectDuration { get; private set; }
    [field: SerializeField] public float EffectDamageFrequency { get; private set; }
}
