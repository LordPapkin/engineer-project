public enum EffectType
{
    Burn,
    Shutdown,
    Heal,
    SpeedUp
}
