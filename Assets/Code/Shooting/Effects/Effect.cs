public abstract class Effect
{
    public Unit affectedUnit;
    public EffectType type;
    public int amount;
    public float frequency;
    public float duration;
    public float currentDelay;

    public abstract void Run();
    public virtual void Start() { }
    public virtual void End() { }
}
