using UnityEngine;

public class HealEffect : Effect
{
    public HealEffect(int healAmount, float frequency, float duration)
    {
        this.amount = healAmount;
        this.frequency = frequency;
        this.duration = duration;
        currentDelay = frequency;
        type = EffectType.Heal;
    }

    public override void Run()
    {
        affectedUnit.TakeHeal(amount);
    }
}
