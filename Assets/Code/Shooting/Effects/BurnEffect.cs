using UnityEngine;

public class BurnEffect : Effect
{
    public BurnEffect(int damage, float frequency, float duration)
    {
        this.amount = damage;
        this.frequency = frequency;
        this.duration = duration;
        currentDelay = frequency;
        type = EffectType.Burn;
    }

    public override void Run()
    {
        affectedUnit.TakeDamage(ElementType.Fire, amount);
    }
}
