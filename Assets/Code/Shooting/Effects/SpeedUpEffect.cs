using UnityEngine;

public class SpeedUpEffect : Effect
{
    private Slime slime;
    
    public SpeedUpEffect(float duration)
    {
        this.duration = duration;
        type = EffectType.SpeedUp;
    }

    public override void Run() { }

    public override void Start()
    {
        slime = affectedUnit as Slime;
        if (slime != null) 
            slime.ChangeSpeed(slime.SlimeData.MovementSpeed * 1.5f);
    }

    public override void End()
    {
        if (slime != null) 
            slime.ChangeSpeed(slime.SlimeData.MovementSpeed);
    }
}
