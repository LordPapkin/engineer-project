using UnityEngine;

public interface IShooter
{
    GameObject ProjectilePrefab { get; set; }
    Transform ProjectileSpawnTransform { get; set; }
}
