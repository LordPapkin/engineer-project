using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObjects/HealthSystem/StrengthsAndWeaknesses")]
public class StrengthsAndWeaknessesSO : ScriptableObject
{
    [field: SerializeField] public ElementType EntityElementType { get; private set; }
    [field: SerializeField] public ElementType ResistElementType { get; private set; }
    [field: SerializeField] public ElementType CounterElementType { get; private set; }
    [field: SerializeField] public float CounterMultiplier { get; private set; } = 1.5f;
    [field: SerializeField] public float ResistMultiplier { get; private set; } = 0.5f;
}
