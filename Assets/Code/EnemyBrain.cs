using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public enum EnemyBehaviourType
{
    Neutral,
    Defensive,
    Offensive
}

[Serializable]
public class EnemyBehaviour
{
    public EnemyBehaviourType behaviourType;

    [Range(0,8)] public byte frontSpawnMultiplier;
    [Range(0, 8)] public byte midSpawnMultiplier;
    [Range(0, 8)] public byte backSpawnMultiplier; 

    public TowerSetData towerSet;
}

public class EnemyBrain : MonoBehaviour
{
    public static EnemyBrain instance;

    //TODO: Subscribe to NewTurn event

    [Header("Enemy Behaviour")]
    [SerializeField] private List<EnemyBehaviour> behaviourSet;
    public EnemyBehaviour currentEnemyBehaviour;

    [Header("Debugging")]
    [SerializeField] private EnemyBehaviourType targetBehavior = EnemyBehaviourType.Neutral;
    [SerializeField] private bool changeBehavior = false; 
    
    
    private int currentTurnTickets;

    void Awake()
    {
        if(instance != null)
            Destroy(this);      
        instance = this;
        
        ChangeEnemyBehaviour(EnemyBehaviourType.Neutral);
    }

    private void Start()
    {
        currentTurnTickets = LevelDatastore.CurrentLevelData.startTurnTickets;
    }

    void Update()
    {
        if(changeBehavior)
        {
            ChangeEnemyBehaviour(targetBehavior);
            changeBehavior = false;
        }
    }

    
    private void OnBattleFinished()
    {
        ChangeEnemyBehaviour();
        currentTurnTickets += LevelDatastore.CurrentLevelData.nextTurnTickets;

        TowerSpawnManager.instance.OnBattleFinished();

        TurnEventSystem.instance.NextState(TurnState.BattleFinished, TurnState.SpawningTowers);
    }


    public void SubscribeToEventSystem()
    {
        TurnEventSystem.instance.Action_BattleFinished += OnBattleFinished;
    }

    public void ChangeEnemyBehaviour()
    {
        if(behaviourSet.Count <= 0)
        {
            Debug.LogError("The behaviour set list is empty. Please add at least one behaviour to proceed.");
            return;
        }

        currentEnemyBehaviour = behaviourSet[UnityEngine.Random.Range(0, behaviourSet.Count)];
    }

    public void ChangeEnemyBehaviour(EnemyBehaviourType desiredbehaviour)
    {
        List<EnemyBehaviour> suitablebehaviour = new List<EnemyBehaviour>();

        foreach (var behaviour in behaviourSet)
            if (behaviour.behaviourType == desiredbehaviour)
                suitablebehaviour.Add(behaviour);

        if (suitablebehaviour.Count <= 0)
        {
            Debug.LogWarning("No suitable behaviour of type " + desiredbehaviour + " been found. Aborting.");
            return;
        }

        currentEnemyBehaviour = suitablebehaviour[UnityEngine.Random.Range(0, suitablebehaviour.Count)];
    }

    public int GetEnemyBrainTickets => currentTurnTickets;
    
    public void EnemyPayTickets(int value) => currentTurnTickets -= value;

}
