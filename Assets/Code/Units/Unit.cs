using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Unit : MonoBehaviour, IDamageable
{
    public virtual List<Effect> CurrentEffects { get; set; }
    public bool IsShutdown { get; set; }

    [SerializeField] protected HealthBar healthBar;

    protected HealthSystem healthSystem;
    private Dictionary<Effect, ParticlesStruct> effectsParticlesDictionary;

    protected virtual void Start()
    {
        CurrentEffects = new();
        effectsParticlesDictionary = new();

        healthSystem = new();
        healthSystem.onHeal += HandleHeal;
        healthSystem.onHit += HandleHit;
        healthSystem.onDeath += HandleDeath;

        StartCoroutine(HandleEffects());
    }

    protected virtual void OnDestroy()
    {
        if (healthSystem == null)
            return;

        healthSystem.onHeal -= HandleHeal;
        healthSystem.onHit -= HandleHit;
        healthSystem.onDeath -= HandleDeath;
    }

    public virtual void TakeDamage(ElementType elementType, int amount)
    {
        healthSystem.TakeDamage(amount, elementType);
    }

    public virtual void TakeHeal(int amount)
    {
        healthSystem.Heal(amount);
    }

    public virtual void TakeEffect(Effect effect)
    {
        if (effect.affectedUnit != null)
            Debug.LogError("Trying to apply same effect object to another unit. This is not allowed. Create new Effect object per unit");

        var existing = CurrentEffects.Find(x => x.type == effect.type);

        if (existing == null)
        {
            effect.affectedUnit = this;
            CurrentEffects.Add(effect);
            effect.Start();
        }
        else
        {
            existing.duration = Mathf.Max(effect.duration, existing.duration);
        }
    }
    
    public virtual void TakeEffect(Effect effect, Particles particleType)
    {
        if (effect.affectedUnit != null)
            Debug.LogError("Trying to apply same effect object to another unit. This is not allowed. Create new Effect object per unit");

        var existing = CurrentEffects.Find(x => x.type == effect.type);

        if (existing == null)
        {
            effect.affectedUnit = this;
            CurrentEffects.Add(effect);
            effect.Start();
            
            var unitTransform = transform;
            var effectsParticles = ParticleManager.Instance.GetParticleSystem(particleType, unitTransform.position);
            effectsParticles.transform.SetParent(unitTransform);
            effectsParticles.Play();
            effectsParticlesDictionary.Add(effect, new ParticlesStruct(particleType, effectsParticles));
        }
        else
        {
            existing.duration = Mathf.Max(effect.duration, existing.duration);
        }
    }

    public virtual void Die()
    {
        Destroy(gameObject);
    }

    protected virtual void HandleDeath()
    {
        Die();
    }

    protected virtual void HandleHit()
    {
        healthBar.UpdateHealthBar(healthSystem.GetHealthRatio());
    }

    protected virtual void HandleHeal()
    {
        healthBar.UpdateHealthBar(healthSystem.GetHealthRatio());
    }

    protected virtual IEnumerator HandleEffects()
    {
        while (gameObject != null && gameObject.activeInHierarchy)
        {
            foreach (var effect in CurrentEffects)
            {
                if (effect.currentDelay <= 0)
                {
                    effect.currentDelay = effect.frequency;
                    effect.Run();
                }

                effect.currentDelay -= Time.deltaTime;
                effect.duration -= Time.deltaTime;
            }

            foreach (var effect in CurrentEffects.FindAll(effect => effect.duration <= 0))
            {
                if (effectsParticlesDictionary.ContainsKey(effect))
                {
                    effectsParticlesDictionary[effect].particleSystem.Stop();
                    ParticleManager.Instance.ReturnParticleSystem(effectsParticlesDictionary[effect].particleSystem, effectsParticlesDictionary[effect].particleType);
                    effectsParticlesDictionary.Remove(effect);
                }
                effect.End();
                CurrentEffects.Remove(effect);
            }

            yield return new WaitForEndOfFrame();
        }
    }
}
