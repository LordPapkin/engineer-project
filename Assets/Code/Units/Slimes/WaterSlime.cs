using PathCreation;
using System.Linq;
using UnityEngine;

public class WaterSlime : Slime
{
    private float shootingCooldown;
    private WaterSlimeData waterSlimeData;
    [SerializeField] private EffectData healEffectData;

    private void Update()
    {
        shootingCooldown -= Time.deltaTime;

        if (shootingCooldown <= 0)
        {
            var targets = FindTargets();
            Shoot(targets);
            shootingCooldown = 1f / waterSlimeData.AttackSpeed;
        }
    }

    public override void Spawn(Spawner spawner, PathCreator startPath)
    {
        base.Spawn(spawner, startPath);

        waterSlimeData = SlimeData as WaterSlimeData;
        shootingCooldown = 1f / waterSlimeData.AttackSpeed;
    }

    public override void Die()
    {
        base.Die();
    }

    void Shoot(Slime[] slimes)
    {
        foreach (var slime in slimes)
        {
            var effect = new HealEffect(healEffectData.EffectValue, healEffectData.EffectDamageFrequency, healEffectData.EffectDuration);
            slime.TakeEffect(effect, Particles.Heal);
        }

        SlimeAnimator.SpecialAttack();
    }

    Slime[] FindTargets()
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, waterSlimeData.Range);

        return colliders
            .Select(x => x.gameObject.GetComponent<Slime>())
            .Where(x => x != null)
            .ToArray();
    }
}
