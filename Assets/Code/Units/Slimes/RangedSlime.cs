using PathCreation;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangedSlime : Slime, IShooter
{
    [field: SerializeField] public GameObject ProjectilePrefab { get; set; }
    [field: SerializeField] public Transform ProjectileSpawnTransform { get; set; }

    private float shootingCooldown;

    private void Update()
    {
        shootingCooldown -= Time.deltaTime;

        if (shootingCooldown <= 0)
        {
            var target = FindTarget();
            if (target != null)
                Shoot(target.transform);
            shootingCooldown = 1f / SlimeData.AttackSpeed;
        }
    }

    public override void Spawn(Spawner spawner, PathCreator startPath)
    {
        base.Spawn(spawner, startPath);

        shootingCooldown = 1f / SlimeData.AttackSpeed;
    }

    public override void Die()
    {
        base.Die();
    }

    void Shoot(Transform target)
    {
        var projectileGameObject = Instantiate(ProjectilePrefab, ProjectileSpawnTransform.position, Quaternion.identity);
        BaseProjectile projectile = projectileGameObject.GetComponent<BaseProjectile>();
        projectile.Create(transform, target, SlimeData.Range, "Tower");

        SlimeAnimator.Attack();
    }

    Tower FindTarget()
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, SlimeData.Range);
        Tower closestTower = null;
        float closestTowerDist = float.MaxValue;

        foreach (Collider coll in colliders)
        {
            Tower enemy = coll.gameObject.GetComponent<Tower>();
            if (enemy == null)
                continue;

            var dist = Vector3.Distance(ProjectileSpawnTransform.position, enemy.transform.position);

            if (dist < closestTowerDist)
            {
                closestTower = enemy;
                closestTowerDist = dist;
            }
        }

        return closestTower;
    }
}
