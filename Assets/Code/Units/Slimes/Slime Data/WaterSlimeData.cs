using UnityEngine;

[CreateAssetMenu(fileName = "New Water Slime Data", menuName = "ScriptableObjects/Combat/Water Slime Data")]
public class WaterSlimeData : SlimeData
{
    [field: SerializeField] public int HealAmount { get; private set; }
}
