using UnityEngine;

[CreateAssetMenu(fileName = "New Thunder Slime Data", menuName = "ScriptableObjects/Combat/Thunder Slime Data")]
public class EarthSlimeData : SlimeData
{
    [field: SerializeField] public int GatherResourceAmount { get; private set; }
    [field: SerializeField] public float GatherResourceFrequency { get; private set; }
    [field: SerializeField] public ResourceType GatherResourceType { get; private set; }
}
