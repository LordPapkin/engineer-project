using UnityEngine;

[CreateAssetMenu(fileName = "New Slime Data", menuName = "ScriptableObjects/Combat/Slime Data")]
public class SlimeData : ScriptableObject
{
    [field: SerializeField] public string SlimeName { get; private set; }
    [field: SerializeField] public int Damage { get; private set; }
    [field: SerializeField] public int ObjectiveDamage { get; private set; }
    [field: SerializeField] public float Range { get; private set; }
    [field: SerializeField] public float AttackSpeed { get; private set; }
    [field: SerializeField] public float EffectDuration { get; private set; }
    [field: SerializeField] public float EffectDamageFrequency { get; private set; }
    [field: SerializeField] public int Cost { get; private set; }
    [field: SerializeField] public int Level { get; private set; }
    [field: SerializeField] public float MovementSpeed { get; private set; }
    [field: SerializeField] public int MaxHealth { get; private set; }
    [field: SerializeField] public StrengthsAndWeaknessesSO StrengthsAndWeaknesses { get; private set; }
    [field: SerializeField] public ElementType ElementType { get; private set; }
    [field: SerializeField] public ResourceAmountArrayDataSO ResourceAmountArrayData { get; private set; }
    [field: SerializeField] public GameObject SlimePrefab { get; private set; }
}
