using PathCreation;
using UnityEngine;

public abstract class Slime : Unit
{
    [field: SerializeField] public SlimeData SlimeData { get; private set; }
    [field: SerializeField] public GameObject GFXPrefab { get; private set; }
    [field: SerializeField] public Material Material { get; private set; }
    protected SlimeAnimationControl SlimeAnimator;
    protected SlimeFacialAnimator FaceAnimator;
    protected PathFollower follower;
    protected Spawner spawner;

    protected override void Start()
    {
        base.Start();
        healthSystem.SetHealthSystem(SlimeData.MaxHealth, SlimeData.StrengthsAndWeaknesses);
        SlimeAnimator = GFXPrefab.GetComponent<SlimeAnimationControl>();
        FaceAnimator = GFXPrefab.GetComponent<SlimeFacialAnimator>();

        SlimeAnimator.Move();
    }

    public virtual void Spawn(Spawner spawner, PathCreator startPath)
    {
        this.spawner = spawner;

        spawner.AddToAliveList(gameObject);

        follower = GetComponent<PathFollower>();
        follower.CurrentPath = startPath;
        follower.SetSpeed(SlimeData.MovementSpeed);
    }

    protected override void HandleHit()
    {
        SFXManager.Instance.PlaySound(SFXManager.SFX.SlimeHit);
        FaceAnimator.Hurt();
        base.HandleHit();
    }

    protected override void HandleHeal()
    {
        ParticleManager.Instance.PlayParticlesOneShot(Particles.Heal, transform.position);
        FaceAnimator.Smile();
        base.HandleHeal();
    }

    public override void Die()
    {
        ParticleManager.Instance.PlayParticlesOneShot(Particles.SlimeDeath, transform.position, Material);
        spawner.RemoveFromAliveList(gameObject);
        SFXManager.Instance.PlaySound(SFXManager.SFX.SlimeDeath);
        FaceAnimator.Hurt();
        base.Die();
    }

    public void DieOnDoors()
    {
        spawner.RemoveFromAliveList(gameObject);
        base.Die();
    }

    public void ChangeSpeed(float newSpeed)
    {
        follower.SetSpeed(newSpeed);
    }
}
