using PathCreation;
using System.Linq;
using UnityEngine;

public class FlameSlime : Slime
{
    [SerializeField] private EffectData burnEffectData;
    private float shootingCooldown;

    private void Update()
    {
        shootingCooldown -= Time.deltaTime;

        if (shootingCooldown <= 0)
        {
            var targets = FindTargets();
            Shoot(targets);
            shootingCooldown = 1f / SlimeData.AttackSpeed;
        }
    }

    public override void Spawn(Spawner spawner, PathCreator startPath)
    {
        base.Spawn(spawner, startPath);

        shootingCooldown = 1f / SlimeData.AttackSpeed;
    }

    public override void Die()
    {
        base.Die();
    }

    void Shoot(Tower[] towers)
    {
        foreach (var tower in towers)
        {
            var effect = new BurnEffect(burnEffectData.EffectValue, burnEffectData.EffectDamageFrequency, burnEffectData.EffectDuration);
            tower.TakeEffect(effect, Particles.Fire);
        }

        SlimeAnimator.SpecialAttack();
    }

    Tower[] FindTargets()
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, SlimeData.Range);

        return colliders
            .Select(x => x.gameObject.GetComponent<Tower>())
            .Where(x => x != null)
            .ToArray();
    }
}
