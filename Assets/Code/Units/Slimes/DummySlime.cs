using PathCreation;
using UnityEngine;

// Dummy slime just spawns, does nothing for 5sec, and dies
public class DummySlime : Slime
{
    [SerializeField] private float lifeTimer = 5f;

    private void Update() {
        lifeTimer -= Time.deltaTime;

        if (lifeTimer <= 0) {
            Die();
        }
    }

    public override void Spawn(Spawner spawner, PathCreator startPath) {
        Debug.Log("Dummy slime spawned");
        base.Spawn(spawner, startPath);
    }

    public override void Die() {
        Debug.Log("Dummy slime died");
        base.Die();
    }
}
