using PathCreation;
using UnityEngine;

public class MotherSlime : Slime
{
    [SerializeField] private GameObject childPrefab;
    [SerializeField] private float childSpawnDelay;

    private float spawningCooldown;

    private void Update()
    {
        spawningCooldown -= Time.deltaTime;

        if (spawningCooldown <= 0)
        {
            SpawnChild();
            spawningCooldown = childSpawnDelay;
        }
    }

    public override void Spawn(Spawner spawner, PathCreator startPath)
    {
        base.Spawn(spawner, startPath);

        spawningCooldown = childSpawnDelay;
    }

    public override void Die()
    {
        base.Die();
    }

    private void SpawnChild()
    {
        var spawnedObj = Instantiate(childPrefab, transform.position, transform.rotation);
        var spawnedFollower = spawnedObj.GetComponent<PathFollower>();
        var spawnedSlime = spawnedObj.GetComponent<Slime>();
        spawnedSlime.Spawn(spawner, follower.CurrentPath);
        spawnedFollower.SetDistanceTraveled(follower.DistanceTravelled);

        SlimeAnimator.Attack();
    }
}
