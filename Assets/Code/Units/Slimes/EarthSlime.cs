using PathCreation;
using UnityEngine;

public class EarthSlime : Slime
{
    private float gatherResourcesCooldown;
    private EarthSlimeData earthSlimeData;

    private void Update()
    {
        gatherResourcesCooldown -= Time.deltaTime;

        if (gatherResourcesCooldown <= 0)
        {
            GatherResources();
            gatherResourcesCooldown = 1f / earthSlimeData.GatherResourceFrequency;
        }
    }

    public override void Spawn(Spawner spawner, PathCreator startPath)
    {
        base.Spawn(spawner, startPath);

        earthSlimeData = SlimeData as EarthSlimeData;
        gatherResourcesCooldown = 1f / earthSlimeData.GatherResourceFrequency;
    }

    void GatherResources()
    {
        var resourceAmount = new ResourceAmount(earthSlimeData.GatherResourceType, earthSlimeData.GatherResourceAmount);
        ResourceManager.Instance.AddResources(resourceAmount);

        SlimeAnimator.Gather();
    }
}
