using System.Collections;
using UnityEngine;

public abstract class Tower : Unit
{
    [field: SerializeField] public GameObject ProjectilePrefab { get; set; }
    [field: SerializeField] public Transform ProjectileSpawnTransform { get; set; }

    public TowerData TowerData => towerData;

    [SerializeField] private TowerData towerData;
    [SerializeField] private float explosionForce;
    [SerializeField] private Animator animator;
    [SerializeField] private GameObject towerTopGFX;
    [SerializeField] private float rotationSpeed;

    private Transform target;
    private TowerSpawnPoint spawnPoint;
    private float rotationLerp;
    private Quaternion rotationQuat;
    
    static readonly int ShootID = Animator.StringToHash("Shoot");

    protected override void Start()
    {
        base.Start();
        healthSystem.SetHealthSystem(towerData.MaxHealth, towerData.StrengthsAndWeaknesses);
        Init();
    }

    private void Update()
    {
        if (target == null)
            return;

        RotateTowardsTarget();
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();
    }

    public virtual void Spawn(TowerSpawnPoint spawn)
    {
        spawnPoint = spawn;
        Debug.Log("Hello! I, " + gameObject.name + " a child of " + spawnPoint + " has been spawned!");
    }

    public void Init()
    {
        StartCoroutine(HandleSearch());
        StartCoroutine(HandleShooting());
    }

    private IEnumerator HandleSearch()
    {
        while (true)
        {
            yield return new WaitForSeconds(1f/towerData.SearchRefreshSpeed);

            if (!IsShutdown)
                LookForTarget();
        }
    }
    
    private void LookForTarget()
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, towerData.Range);  

        foreach (Collider coll in colliders)
        {
            Slime enemy = coll.gameObject.GetComponent<Slime>();
            if (enemy == null)
            {
                target = null;
                continue;
            }

            if (target == null)
            {
                target = enemy.transform;
                continue;
            }

            if (Vector3.Distance(transform.position, enemy.transform.position) < Vector3.Distance(transform.position, target.transform.position))
            {
                target = enemy.transform;
            }
        }

        if (target != null)
        {
            animator.SetBool("IsTargeting", true);
            rotationLerp = 0f;
        }
        else
            animator.SetBool("IsTargeting", false);
    }

    private IEnumerator HandleShooting()
    {
        while (true)
        {
            yield return new WaitForSeconds(1f / towerData.AttackSpeed);

            if (!IsShutdown)
                TryToShoot();
        }
    }

    private void TryToShoot()
    {
        if (target == null)
            return;
        
        Shoot();
    }

    private void Shoot()
    {
        animator.SetTrigger(ShootID);
        GameObject projectileGameObject = Instantiate(ProjectilePrefab, ProjectileSpawnTransform.position, Quaternion.identity, ProjectileSpawnTransform);
        BaseProjectile projectile = projectileGameObject.GetComponent<BaseProjectile>();
        projectile.Create(transform, target, towerData.Range, "Slime");
    }
    
    public override void Die()
    {
        if (IsShutdown)
            return;

        healthBar.gameObject.SetActive(false);
        ResourceManager.Instance.AddResources(towerData.DestroyedTowerReward);
        spawnPoint?.StateDestroyed();
        ParticleManager.Instance.PlayParticlesOneShot(Particles.Explosion, transform.position);

        IsShutdown = true;
        Rigidbody rigid = towerTopGFX.AddComponent<Rigidbody>();
        rigid.AddForce(Vector3.up * explosionForce, ForceMode.Impulse);
        StartCoroutine(Died()); 
    }

    IEnumerator Died()
    {
        yield return new WaitForSeconds(1f);
        base.Die();
    }

    protected override void HandleHit()
    {
        base.HandleHit();
        SFXManager.Instance.PlaySound(SFXManager.SFX.TowerHit);
    }

    private void RotateTowardsTarget()
    {
        if (rotationLerp == 0)
           rotationQuat = towerTopGFX.transform.rotation;

        var lookAtTarget = new Vector3(target.position.x, transform.position.y, target.position.z);
        towerTopGFX.transform.LookAt(lookAtTarget, Vector3.up);
        towerTopGFX.transform.rotation = Quaternion.Lerp(rotationQuat, towerTopGFX.transform.rotation, rotationLerp);

        if (rotationLerp < 1f)
            Mathf.Clamp(rotationLerp += Time.deltaTime * rotationSpeed, 0, 1);
    }
}
