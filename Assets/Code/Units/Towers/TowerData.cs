using UnityEngine;

[CreateAssetMenu(fileName = "New Tower Data", menuName = "ScriptableObjects/Combat/Tower Data")]
public class TowerData : ScriptableObject
{
    [field: SerializeField] public string TowerName { get; private set; }
    [field: SerializeField] public float Range { get; private set; }
    [field: SerializeField] public float SearchRefreshSpeed { get; private set; }
    [field: SerializeField] public float AttackSpeed { get; private set; }
    [field: SerializeField] public int Cost { get; private set; }
    [field: SerializeField] public int Level { get; private set; }
    [field: SerializeField] public int MaxHealth { get; private set; }
    [field: SerializeField] public StrengthsAndWeaknessesSO StrengthsAndWeaknesses { get; private set; }
    [field: SerializeField] public ElementType ElementType { get; private set; }
    [field: SerializeField] public ResourceAmount[] DestroyedTowerReward { get; private set; }
}
