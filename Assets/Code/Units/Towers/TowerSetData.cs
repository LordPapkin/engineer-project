using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CreateAssetMenu(fileName = "New Tower Data", menuName = "ScriptableObjects/Combat/TowerSet Data")]
public class TowerSetData : ScriptableObject
{
    public List<Tower> TowerSetList = new List<Tower>();
}
