using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerSpawnPoint : MonoBehaviour
{
    public enum TurretSpawnPointStates
    {
        Empty,
        Destroyed,
        Spawned
    }

    public enum TurretSpawnPointTypes
    {
        Front,
        Mid,
        Back
    }

    [SerializeField] private TurretSpawnPointStates CurrentTurretSpawnState = TurretSpawnPointStates.Empty;
    [SerializeField] private TurretSpawnPointTypes TurretSpawnPointType = TurretSpawnPointTypes.Front;

    [Header("Debugging")]
    [SerializeField][Range(0.1f, 1f)] private float gizmoSize = 1f;


    private void OnDrawGizmos()
    {
        switch (CurrentTurretSpawnState)
        {
            case TurretSpawnPointStates.Destroyed:
                Gizmos.color = Color.black;
                break;
            case TurretSpawnPointStates.Empty:
                Gizmos.color = Color.blue;
                break;
            case TurretSpawnPointStates.Spawned:
                Gizmos.color = Color.green;
                break;
        }
        Gizmos.DrawSphere(gameObject.transform.position, 0.25f * gizmoSize);

        switch (TurretSpawnPointType)
        {
            case TurretSpawnPointTypes.Front:
                Gizmos.color = Color.red;
                break;
            case TurretSpawnPointTypes.Mid:
                Gizmos.color = Color.yellow;
                break;
            case TurretSpawnPointTypes.Back:
                Gizmos.color = Color.green;
                break;
        }
        Gizmos.DrawWireSphere(gameObject.transform.position, 0.3f * gizmoSize);
    }


    public void StateEmpty() => CurrentTurretSpawnState = TurretSpawnPointStates.Empty;
    public void StateDestroyed() => CurrentTurretSpawnState = TurretSpawnPointStates.Destroyed;
    public void StateSpawned() => CurrentTurretSpawnState = TurretSpawnPointStates.Spawned;
    public TurretSpawnPointTypes GetTurretSpawnType() => TurretSpawnPointType;
    public TurretSpawnPointStates GetTurretSpawnState() => CurrentTurretSpawnState;
}
