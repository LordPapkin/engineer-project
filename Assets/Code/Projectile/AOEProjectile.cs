using System.Linq;
using UnityEngine;

public class AOEProjectile : BaseProjectile
{
    protected override void Hit(Collider collision)
    {
        ParticleManager.Instance.PlayParticlesOneShot(Particles.ExplosionSmall, collision.transform.position);
        var targets = FindTargets();
        foreach (var target in targets)
        {
            var slime = target.GetComponent<Slime>();
            if(slime != null)
                slime.TakeDamage(attackType, dmg);
        }
        Hide();
    }
    
    Collider[] FindTargets()
    {
        return Physics.OverlapSphere(transform.position, 2f).Where(x => x.CompareTag("Slime")).ToArray();
    }
}