using UnityEngine;

public class ThunderProjectile : BaseProjectile
{
    private int hitTargets = 0;

    protected override void Hit(Collider collision)
    {
        if (!collision.CompareTag(targetTag))
            return;

        var obj = collision.GetComponent<MonoBehaviour>();
        if (obj is IDamageable damageable)
        {
            hitTargets++;
            targetEnemy = null;
            DealDamage(damageable);
        }
    }

    protected override void DealDamage(IDamageable damageable)
    {
        damageable.TakeDamage(attackType, dmg/hitTargets);
        if (hitTargets < 3)
        {
            if(!LookForNextTarget())
                Hide();
        }
        else
        {
            Hide();
        }
    }
    
    private bool LookForNextTarget()
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, 3f);

        foreach (Collider coll in colliders)
        {
            Slime enemy = coll.gameObject.GetComponent<Slime>();
            if (enemy == null)
                continue;
            if (targetEnemy == null)
            {
                targetEnemy = enemy.transform;
                continue;
            }
            if (Vector3.Distance(transform.position, enemy.transform.position) < Vector3.Distance(transform.position, targetEnemy.transform.position))
            {
                targetEnemy = enemy.transform;
            }
        }
        
        return targetEnemy != null;
    }
}
