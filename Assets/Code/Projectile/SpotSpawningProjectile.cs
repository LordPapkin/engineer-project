using UnityEngine;

public class SpotSpawningProjectile : BaseProjectile
{
    [SerializeField] private GameObject effectSpot;

    protected override void Hit(Collider collision)
    {
        if (!collision.CompareTag(targetTag))
            return;

        var obj = collision.GetComponent<MonoBehaviour>();
        if (obj is IDamageable damageable)
        {
            DealDamage(damageable);
            var spawnedSpot = Instantiate(effectSpot, transform.position, Quaternion.identity);
            Destroy(spawnedSpot, 2f);
        }
    }
}
