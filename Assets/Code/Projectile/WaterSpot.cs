using UnityEngine;

public class WaterSpot : MonoBehaviour
{
    [SerializeField] private EffectData slowDownEffectData;
    
    private void OnTriggerEnter(Collider collision)
    {
        if (!collision.CompareTag("Slime"))
            return;
        
        var obj = collision.GetComponent<MonoBehaviour>();
        if (obj is IDamageable damageable)
        {
            DealDamage(damageable);
        }
    }
    
    private void DealDamage(IDamageable damageable)
    {
        var effect = new SlowDownEffect(slowDownEffectData.EffectDuration);
        damageable.TakeEffect(effect, Particles.SlowDown);
    }
}
