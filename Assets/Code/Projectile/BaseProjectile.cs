using System.Collections;
using UnityEngine;

public class BaseProjectile : MonoBehaviour
{
    [SerializeField] protected int dmg;
    [SerializeField] protected ElementType attackType;
    [SerializeField] protected float moveSpeed;
    [SerializeField] private float timeToDie;
    protected Vector3 sourcePos;
    protected Vector3 moveDir;
    protected Transform targetEnemy;
    protected float range;
    protected string targetTag;

    private void OnTriggerEnter(Collider collision)
    {
        Hit(collision);
    }

    private void Start()
    {
        if (targetEnemy == null || targetEnemy.gameObject == null || gameObject == null)
        {
            Destroy(gameObject);
            return;
        }

        StartCoroutine(DisableAfterTime());
    }

    private void Update()
    {
        MoveTowardsTarget();
    }

    public virtual void SetTarget(Transform target)
    {
        targetEnemy = target;

        if (targetEnemy == null || targetEnemy.gameObject == null || gameObject == null)
        {
            Destroy(gameObject);
            return;
        }

        CalculateMoveDir();
    }

    public void Create(Transform source, Transform targetEnemy, float range, string targetTag)
    {
        this.sourcePos = source.position;
        this.targetTag = targetTag;
        this.range = range;
        SetTarget(targetEnemy);
    }

    protected void Hide()
    {
        gameObject.SetActive(false);
    }

    protected virtual void Hit(Collider collision)
    {
        if (!collision.CompareTag(targetTag))
            return;

        var obj = collision.GetComponent<MonoBehaviour>();
        if (obj is IDamageable damageable)
        {
            DealDamage(damageable);
            Hide();
        }
    }

    protected virtual void MoveTowardsTarget()
    {
        if (targetEnemy == null || targetEnemy.gameObject == null || gameObject == null)
        {
            Destroy(gameObject);
            return;
        }

        CalculateMoveDir();
        transform.position += moveDir * (moveSpeed * Time.deltaTime);

        if (Vector3.Distance(sourcePos, targetEnemy.position) >= range + 1)
        {
            Hide();
        }
    }

    protected void CalculateMoveDir()
    {
        moveDir = (targetEnemy.transform.position - transform.position).normalized;
    }

    protected virtual void DealDamage(IDamageable damageable)
    {
        damageable.TakeDamage(attackType, dmg);
        Hide();
    }

    private IEnumerator DisableAfterTime()
    {
        yield return new WaitForSeconds(timeToDie);
        Hide();
    }
}
