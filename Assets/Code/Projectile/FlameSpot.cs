using UnityEngine;

public class FlameSpot : MonoBehaviour
{
    [SerializeField] private EffectData burnEffectData;
    
    private void OnTriggerEnter(Collider collision)
    {
        if (!collision.CompareTag("Slime"))
            return;
        
        var obj = collision.GetComponent<MonoBehaviour>();
        if (obj is IDamageable damageable)
        {
            DealDamage(damageable);
        }
    }
    
    private void DealDamage(IDamageable damageable)
    {
        var effect = new BurnEffect(burnEffectData.EffectValue, burnEffectData.EffectDamageFrequency, burnEffectData.EffectDuration);
        damageable.TakeEffect(effect, Particles.FireSmall);
    }
}
