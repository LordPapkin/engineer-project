using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuDollyCart : MonoBehaviour
{
    public CinemachineDollyCart cart;
    public List<GameObject> list = new List<GameObject>();
    public float CartReset;
    private GameObject currSlime;

    private void Start()
    {
        currSlime = list[Random.Range(0, list.Count)];
        currSlime.SetActive(true);
    }

    void Update()
    {
        if(cart.m_Position > CartReset)
            ResetCart();
    }

    private void ResetCart()
    {
        currSlime.SetActive(false);
        currSlime = list[Random.Range(0, list.Count)];
        currSlime.SetActive(true);
        cart.m_Position = 0;
    }
}
