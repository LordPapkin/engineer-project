using UnityEngine;

[CreateAssetMenu(fileName = "Level", menuName = "ScriptableObjects/Level Data")]
public class LevelData : ScriptableObject
{
    public string sceneName;
    public int maxTurns;
    public int slimesPassedToWin;
    public ResourceAmount[] resourcesGainedPerTurn;
    public int startTurnTickets;
    public int nextTurnTickets;
    public ResourceAmountArrayDataSO startingResources;
}
