using System;
using UnityEngine;
using UnityEngine.Events;

[Serializable]
public enum TurnState
{
    SpawningTowers,
    SlimeAndPathSelecting,
    BattleInProgress,
    BattleFinished 
}

public class TurnEventSystem : MonoBehaviour
{
    public static TurnEventSystem instance;

    public TurnState Current;
    public int TurnNumber = 0;

    private bool isBeforeStart = true;

    [SerializeField] private Spawner spawner;

    [Header("Debugging")]
    [SerializeField] private TurnState correctNext;
    [SerializeField] private bool next = false;
    [SerializeField] private TurnState[] lookUpTable;

    public UnityAction Action_SpawnTowers;
    public UnityAction Action_SlimeAndPathSelecting;
    public UnityAction Action_BattleStart;
    public UnityAction Action_BattleFinished;

    private void Awake()
    {
        if (instance != null)
            Destroy(gameObject);

        instance = this;

        Current = TurnState.BattleFinished;
        correctNext = TurnState.SpawningTowers;
        lookUpTable = (TurnState[])Enum.GetValues(typeof(TurnState));
        TurnNumber = 0;     

        Action_SpawnTowers += DebugConsole;
        Action_SlimeAndPathSelecting += DebugConsole;
        Action_BattleStart += DebugConsole;
        Action_BattleFinished += DebugConsole;
    }

    private void Start()
    {
        MakeEverythingSubscribeToEvents();
    }

    //TODO: Implement logic to move to next state.
    private void Update()
    {
        if(isBeforeStart)
        {
            isBeforeStart = false;
            instance.NextState(TurnState.BattleFinished, TurnState.SpawningTowers);
        }

        if(next)
        {
            next = false;
            MoveToNext();
        }
    }

    // If anything breaks, probably change this method.
    // It is how it is to prevent rapid change of states, in case a few instances calling this method at the same time.
    public bool NextState(TurnState CurrentState, TurnState NextState)
    {
        if (CurrentState != Current && NextState != correctNext)
            return false;

        MoveToNext();
        return true;
    }

    private void EventSender(TurnState state)
    {
        switch (state)
        {
            case TurnState.SpawningTowers:
                TurnNumber++;
                Action_SpawnTowers();
                break;

            case TurnState.SlimeAndPathSelecting:
                Action_SlimeAndPathSelecting();
                break;

            case TurnState.BattleInProgress:
                Action_BattleStart();
                break;

            case TurnState.BattleFinished:
                Action_BattleFinished();
                break;

            default:
                Debug.LogWarning("There is no suitable event for " + state + "state. Please edit EventSender method if it should.");
                break;
        }
    }

    public void MoveToNext()
    {
        Current = correctNext;
        correctNext = FindNext();
        EventSender(Current);
    }

    private TurnState FindNext()
    {
        int targetIndex = CurrentStateInt() + 1;

        if (targetIndex >= lookUpTable.Length)
            targetIndex = 0;

        return lookUpTable[targetIndex];
    }

    private int CurrentStateInt()
    {
        return (int)Current;
    }

    private void DebugConsole()
    {
        Debug.Log("State: " + Current + ". Next state should be: " + correctNext);
    }

    private void MakeEverythingSubscribeToEvents()
    {
        EnemyBrain.instance.SubscribeToEventSystem();
        TowerSpawnManager.instance.SubscribeToEventSystem();
        spawner.SubscribeToEventSystem();
    }
}
