using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlimeDeathParticleMaterial : MonoBehaviour
{
    [SerializeField] ParticleSystem main;
    [SerializeField] ParticleSystem subemiter;

    [SerializeField] bool test;
    [SerializeField] Material material;

    // Update is called once per frame
    void Update()
    {
        if (!test)
            return;

        SetMaterial(material);
        PlayParticle();
        test = false;
    }

    public void SetMaterial(Material material)
    {
        main.GetComponent<ParticleSystemRenderer>().material = material;
        if(subemiter != null)
            subemiter.GetComponent<ParticleSystemRenderer>().material = material;
    }

    public void PlayParticle()
    {
        main.Play();
    }
}
