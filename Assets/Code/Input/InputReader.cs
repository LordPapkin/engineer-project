using System;
using UnityEngine;
using UnityEngine.InputSystem;

[CreateAssetMenu(fileName = "InputReader", menuName = "ScriptableObjects/Input/InputReader")]
public class InputReader : ScriptableObject
{
    private GameInput gameInput;

    private void OnEnable()
    {
        if (gameInput == null)
        {
            gameInput = new GameInput();
        }

        gameInput.Gameplay.Enable();
    }

    private void OnDisable()
    {
        gameInput.Gameplay.Disable();
    }

    public Vector2 GetMoveVector()
    {
        return gameInput.Gameplay.Move.ReadValue<Vector2>();
    }

    public Vector2 MousePosition()
    {
        return gameInput.Gameplay.MousePosition.ReadValue<Vector2>();
    }
    
    public event Action<InputAction.CallbackContext> RightMouseClick
    {
        add => gameInput.Gameplay.RightMouseClick.performed += value;
        remove => gameInput.Gameplay.RightMouseClick.performed -= value;
    }
    
    public event Action<InputAction.CallbackContext> LeftMouseClick
    {
        add => gameInput.Gameplay.LeftMouseClick.performed += value;
        remove => gameInput.Gameplay.LeftMouseClick.performed -= value;
    }
    
    public event Action<InputAction.CallbackContext> OpenMenu
    {
        add => gameInput.Gameplay.OpenMenu.performed += value;
        remove => gameInput.Gameplay.OpenMenu.performed -= value;
    }
}
