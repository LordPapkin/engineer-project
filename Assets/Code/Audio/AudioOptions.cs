using System;
using UnityEngine;
using UnityEngine.UI;

public class AudioOptions : MonoBehaviour
{
    [SerializeField] private Slider sfxSlider;
    [SerializeField] private Slider musicSlider;
    [SerializeField] private Slider masterSlider;

    public static Action<float> masterVolumeChanged;
    public static Action<float> sfxVolumeChanged;
    public static Action<float> musicVolumeChanged;

    private void Awake()
    {
        sfxSlider.onValueChanged.AddListener(HandleSfxVolumeSliderChanged); 
        musicSlider.onValueChanged.AddListener(HandleMusicVolumeSliderChanged); 
        masterSlider.onValueChanged.AddListener(HandleMasterVolumeSliderChanged);

        masterSlider.value = PlayerPrefs.GetFloat("masterVolume", 0.5f);
        musicSlider.value = PlayerPrefs.GetFloat("musicVolume", 0.5f);
        sfxSlider.value = PlayerPrefs.GetFloat("sfxVolume", 0.5f);
    }

    private void OnDestroy()
    {
        sfxSlider.onValueChanged.RemoveAllListeners();
        musicSlider.onValueChanged.RemoveAllListeners();
        masterSlider.onValueChanged.RemoveAllListeners();
    }

    private static void HandleSfxVolumeSliderChanged(float value)
    {
        PlayerPrefs.SetFloat("sfxVolume", value);
        sfxVolumeChanged?.Invoke(value);
    }
    
    private void HandleMusicVolumeSliderChanged(float value)
    {
        PlayerPrefs.SetFloat("musicVolume", value);
        musicVolumeChanged?.Invoke(value);
    }

    private void HandleMasterVolumeSliderChanged(float value)
    {
        PlayerPrefs.SetFloat("masterVolume", value);
        masterVolumeChanged?.Invoke(value);
    }
}
