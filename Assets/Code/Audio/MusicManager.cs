using UnityEngine;

public class MusicManager : MonoBehaviour
{
    public static MusicManager Instance => instance;
    private static MusicManager instance;

    [SerializeField] private AudioSource musicAudioSource;
    
    private float masterVolume;
    private float musicVolume;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        masterVolume = PlayerPrefs.GetFloat("masterVolume", 1f);
        musicVolume = PlayerPrefs.GetFloat("musicVolume", 0.5f);
        musicAudioSource.volume = musicVolume * masterVolume;
    }

    private void Start()
    {
        AudioOptions.musicVolumeChanged += HandleMusicVolumeChanged;
        AudioOptions.masterVolumeChanged += HandleMasterVolumeChanged;
    }

    private void OnDestroy()
    {
        AudioOptions.musicVolumeChanged -= HandleMusicVolumeChanged;
        AudioOptions.masterVolumeChanged -= HandleMasterVolumeChanged;
    }

    private void HandleMusicVolumeChanged(float value)
    {
        musicVolume = value;
        musicAudioSource.volume = musicVolume * masterVolume;
    }

    private void HandleMasterVolumeChanged(float value)
    {
        masterVolume = value;
        musicAudioSource.volume = musicVolume * masterVolume;
    }
}