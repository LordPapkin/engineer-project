using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SFXManager : MonoBehaviour
{
    public enum SFX 
    { 
        SlimeHit,
        SlimeDeath,
        TowerHit
    }

    public static SFXManager Instance => instance;
    private static SFXManager instance;

    [SerializeField] private AudioSource audioSource;
    private Dictionary<SFX, AudioClip> audioClipDictionary;

    private float masterVolume;
    private float sfxVolume;
    
    public void PlaySound(SFX sound)
    {
        audioSource.PlayOneShot(audioClipDictionary[sound], sfxVolume * masterVolume);
    }

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        masterVolume = PlayerPrefs.GetFloat("masterVolume", 1f);
        sfxVolume = PlayerPrefs.GetFloat("sfxVolume", 0.5f);
        LoadSounds();
    }
    
    private void Start()
    {
        AudioOptions.sfxVolumeChanged += HandleSfxVolumeChanged;
        AudioOptions.masterVolumeChanged += HandleMasterVolumeChanged;
    }

    private void OnDestroy()
    {
        AudioOptions.sfxVolumeChanged -= HandleSfxVolumeChanged;
        AudioOptions.masterVolumeChanged -= HandleMasterVolumeChanged;
    }

    private void HandleSfxVolumeChanged(float value)
    {
        sfxVolume = value;
    }

    private void HandleMasterVolumeChanged(float value)
    {
        masterVolume = value;
    }

    private void LoadSounds()
    {
        audioClipDictionary = new Dictionary<SFX, AudioClip>();
        foreach (SFX sound in System.Enum.GetValues(typeof(SFX)))
        {
            audioClipDictionary[sound] = Resources.Load<AudioClip>(sound.ToString());
        }
    }
}

