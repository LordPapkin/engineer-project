public enum ResourceType {
    Biomass,
    Energy,
    Fire,
    Water
}
