using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObjects/Resources/ResourceAmountData")]
public class ResourceAmountDataSO : ScriptableObject
{
    [field: SerializeField] public ResourceAmount ResourceAmountData { private set; get; }

    private void OnValidate()
    {
        ValidateUtilities.ValidateResourceAmountField(this, nameof(ResourceAmountData), ResourceAmountData);
    }
}