using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObjects/Resources/ResourcesListSO")]
public class ResourceListSO : ScriptableObject
{
    [field: SerializeField] public List<ResourceSO> List { private set; get; }

    private void OnValidate()
    {
        ValidateUtilities.ValidateEnumerableField(this, nameof(List), List);
    }
}
