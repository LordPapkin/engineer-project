using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObjects/Resources/ResourceAmountArrayData")]
public class ResourceAmountArrayDataSO : ScriptableObject
{
    [field: SerializeField] public ResourceAmount[] ResourceAmountData { private set; get; }

    private void OnValidate()
    {
        ValidateUtilities.ValidateResourceAmountField(this, nameof(ResourceAmountData), ResourceAmountData);
    }
}
