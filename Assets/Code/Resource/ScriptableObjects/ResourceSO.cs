using System;
using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObjects/Resources/ResourceSO")]
public class ResourceSO : ScriptableObject
{
   [field: SerializeField] public string ResourceName { private set; get; }

   private void OnValidate()
   {
      ValidateUtilities.ValidateStringField(this, nameof(ResourceName), ResourceName);
   }
}
