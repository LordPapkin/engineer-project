using System;
using System.Collections;
using UnityEngine;

[Serializable]
public struct ResourceAmount
{
    [field: SerializeField] public ResourceType ResourceType { private set; get; }
    [field: SerializeField] public int Amount { private set; get; }

    public ResourceAmount(ResourceType resourceType, int amount) {
        ResourceType = resourceType;
        Amount = amount;
    }
}
