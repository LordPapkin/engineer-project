using UnityEngine;

public class TurnEndResourceAdder : MonoBehaviour
{
    private void Start()
    {
        TurnEventSystem.instance.Action_BattleFinished += AddTurnEndResources;
    }

    private void OnDestroy()
    {
        TurnEventSystem.instance.Action_BattleFinished -= AddTurnEndResources;
    }

    private void AddTurnEndResources()
    {
        ResourceManager.Instance.AddResources(LevelDatastore.CurrentLevelData.resourcesGainedPerTurn);
    }
}
