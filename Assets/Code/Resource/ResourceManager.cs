using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ResourceManager : MonoBehaviour
{
    public static ResourceManager Instance { private set; get; }

    public Action<ResourceOperation> onResourceAmountChanged;

    private const int maxResources = 9999; 
    
    public Dictionary<ResourceType, int> resourcesDictionary;
    
    private void Awake()
    {
        CreateInstance();
        InitDictionary();
    }

    private void Start()
    {
        LoadStartingResources();
        onResourceAmountChanged?.Invoke(null);
    }

    private void OnDestroy()
    {
        Instance = null;
    }

    public void AddResources(ResourceAmount resourceAmount)
    {
        var resource = resourceAmount.ResourceType;
        resourcesDictionary[resource] += resourceAmount.Amount;
        resourcesDictionary[resource] = Math.Clamp(resourcesDictionary[resource], 0, maxResources);

        var operation = new ResourceOperation(resourceAmount.ResourceType, resourceAmount.Amount);
        onResourceAmountChanged?.Invoke(operation);
    }

    public void AddResources(ResourceAmount[] resourceAmounts)
    {
        foreach (var resourceAmount in resourceAmounts)
        {
            var resource = resourceAmount.ResourceType;
            resourcesDictionary[resource] += resourceAmount.Amount;
            resourcesDictionary[resource] = Math.Clamp(resourcesDictionary[resource], 0, maxResources);

            var operation = new ResourceOperation(resourceAmount.ResourceType, resourceAmount.Amount);
            onResourceAmountChanged?.Invoke(operation);
        }
    }

    public bool TrySpendResources(ResourceAmount resourceAmount)
    {
        if (resourcesDictionary[resourceAmount.ResourceType] < resourceAmount.Amount)
            return false;
        
        SpendResources(resourceAmount);

        var operation = new ResourceOperation(resourceAmount.ResourceType, -resourceAmount.Amount);
        onResourceAmountChanged?.Invoke(operation);
        return true;
    }
    
    public bool TrySpendResources(ResourceAmount[] resourceAmounts)
    {
        foreach (var resourceAmount in resourceAmounts)
        {
            if (resourcesDictionary[resourceAmount.ResourceType] < resourceAmount.Amount)
                return false;
        }
        
        SpendResources(resourceAmounts);

        foreach (var resourceAmount in resourceAmounts)
        {
            var operation = new ResourceOperation(resourceAmount.ResourceType, -resourceAmount.Amount);
            onResourceAmountChanged?.Invoke(operation);
        }

        return true;
    }

    public int GetResourceAmount(ResourceType resource)
    {
        return resourcesDictionary[resource];
    }

    public int GetAvailableUnitsNumber(ResourceAmount[] resourcesAmounts)
    {
        return resourcesAmounts.Select(resource => resourcesDictionary[resource.ResourceType] / resource.Amount).Prepend(int.MaxValue).Min();
    }

    private void CreateInstance()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }
    
    private void InitDictionary()
    {
        resourcesDictionary = new Dictionary<ResourceType, int>();
        foreach (ResourceType resource in Enum.GetValues(typeof(ResourceType))) 
        {
            resourcesDictionary.Add(resource, 0);
        }
    }

    private void LoadStartingResources()
    {
        if (LevelDatastore.CurrentLevelData.startingResources == null)
            return;
        
        foreach (var item in LevelDatastore.CurrentLevelData.startingResources.ResourceAmountData)
        {
            resourcesDictionary[item.ResourceType] += item.Amount;
        }
    }

    private void SpendResources(ResourceAmount resourceAmount)
    {
        resourcesDictionary[resourceAmount.ResourceType] -= resourceAmount.Amount;
    }

    private void SpendResources(ResourceAmount[] resourcesAmount)
    {
        foreach (var item in resourcesAmount)
        {
            resourcesDictionary[item.ResourceType] -= item.Amount;
        }
    }
}

public class ResourceOperation
{
    public ResourceType resourceType;
    public int signedAmount;

    public ResourceOperation(ResourceType resourceType, int signedAmount)
    {
        this.resourceType = resourceType;
        this.signedAmount = signedAmount;
    }
}
