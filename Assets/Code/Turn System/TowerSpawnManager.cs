using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class TowerSpawnManager : MonoBehaviour
{
    public static TowerSpawnManager instance;

    //TODO: Subscribe to NewTurn and SpawnTurrets events.
    [Header("Spawnpoint List")]
    [SerializeField] private List<TowerSpawnPoint> spawnPoints = new List<TowerSpawnPoint>();
    [SerializeField] private List<GameObject> hiddenTowerPrefab = new List<GameObject>();

    [Header("Debugging")]
    [SerializeField] private bool spawnButton = false;
    [SerializeField] private bool turnButton = false;
    [SerializeField] private bool nukeAllTowers = false;
    [SerializeField] private List<TowerSpawnPoint> currentSpawnpointPool = new List<TowerSpawnPoint>();
    [SerializeField] private List<Tower> spawnedTowers = new List<Tower>();
    [SerializeField] private List<(Tower, GameObject)> hiddenTowers = new List<(Tower, GameObject)>();

    [Header("ConsoleLogs")]
    [SerializeField] private bool showLogs = true;
    [SerializeField] private bool showWarnings = true;

    private List<TowerSpawnPoint> frontline = new List<TowerSpawnPoint>();
    private List<TowerSpawnPoint> midline = new List<TowerSpawnPoint>();
    private List<TowerSpawnPoint> backline = new List<TowerSpawnPoint>();

    void Awake()
    {
        if (instance != null)
            Destroy(this);

        instance = this;
    }

    private void Start()
    {
        SortSpawnPoints();
        UpdateCurrentSpawnPointPool();
    }

    void Update()
    {
        if (spawnButton)
        {
            if (showLogs)
                Debug.Log("Tower SpawnButton: Click!");
            OnSpawnTurrets();
            spawnButton = false;
        }

        if (turnButton)
        {
            if (showLogs)
                Debug.Log("Tower TurnButton: Click!");
            OnBattleFinished();
            turnButton = false;
        }

        if(nukeAllTowers)
        {
            if (showLogs) 
                Debug.Log("Tower NukeAll: Click!");
            KillAllTurrets();
            nukeAllTowers = false;
        }
    }

    public void SubscribeToEventSystem()
    {
        TurnEventSystem.instance.Action_SpawnTowers += OnSpawnTurrets;
        TurnEventSystem.instance.Action_BattleStart += OnBattleStart;
    }

    public void OnBattleFinished()
    {
        ChangeDestroyedToEmpty();
    }

    public void OnSpawnTurrets()
    {
        UpdateCurrentSpawnPointPool();
        SpawnAllTurrets();

        if (!TurnEventSystem.instance.NextState(TurnState.SpawningTowers, TurnState.SlimeAndPathSelecting))
            Debug.LogWarning(instance +": Failed to change state from " + TurnState.SpawningTowers +" to " + TurnState.SlimeAndPathSelecting);    
    }

    public void OnBattleStart()
    {
        foreach(var unhide in hiddenTowers)
        {
            Destroy(unhide.Item2);
            unhide.Item1.gameObject.SetActive(true);
            //todo change it uwu
            //unhide.Item1.Init();
            spawnedTowers.Add(unhide.Item1);
        }

        hiddenTowers.Clear();
    }

    public void AddSpawnPoint(TowerSpawnPoint spawnPoint)
    {
        spawnPoints.Add(spawnPoint);
    }

    public bool SpawnTowerProcedure()
    {
        if (currentSpawnpointPool.Count <= 0)
        {
            if (showWarnings)
                Debug.LogWarning("Suitable TowerSpawnPoint could not be found. Aborting tower spawning.");
            return false;
        }

        TowerSpawnPoint spawnPoint = currentSpawnpointPool[Random.Range(0, currentSpawnpointPool.Count)];

        Tower towerToSpawn;

        if (!DoesSuitableTowerExist(out towerToSpawn))
        {
            if (showWarnings)
                Debug.LogWarning("Suitable TowerToSpawn could not be found. Aborting tower spawning.");
            return false;
        }

        SpawnSingleTower(spawnPoint, towerToSpawn);
        spawnPoint.StateSpawned();
        int debugRemovedFromList = 0;
        while (currentSpawnpointPool.Remove(spawnPoint))
            debugRemovedFromList++;

        if(showLogs)
            Debug.Log("I have removed " + debugRemovedFromList + " spawn points from spawn pool.");

        return true;
    }


    private void UpdateCurrentSpawnPointPool()
    {
        currentSpawnpointPool.Clear();

        foreach (var turr in frontline)
            if (turr.GetTurretSpawnState() == TowerSpawnPoint.TurretSpawnPointStates.Empty)
                for(int i = 0; i < EnemyBrain.instance.currentEnemyBehaviour.frontSpawnMultiplier ; i++)
                    currentSpawnpointPool.Add(turr);

        foreach (var turr in midline)
            if (turr.GetTurretSpawnState() == TowerSpawnPoint.TurretSpawnPointStates.Empty)
                for (int i = 0; i < EnemyBrain.instance.currentEnemyBehaviour.midSpawnMultiplier; i++)
                    currentSpawnpointPool.Add(turr);

        foreach (var turr in backline)
            if (turr.GetTurretSpawnState() == TowerSpawnPoint.TurretSpawnPointStates.Empty)
                for (int i = 0; i < EnemyBrain.instance.currentEnemyBehaviour.backSpawnMultiplier; i++)
                    currentSpawnpointPool.Add(turr);
    }

    private bool DoesSuitableTowerExist(out Tower tower)
    {
        List<Tower> suitableTowers = new List<Tower>();

        foreach (Tower turr in EnemyBrain.instance.currentEnemyBehaviour.towerSet.TowerSetList)
            if (turr.TowerData.Cost <= EnemyBrain.instance.GetEnemyBrainTickets)
                suitableTowers.Add(turr);

        if (suitableTowers.Count <= 0)
        {
            tower = null;
            return false;
        }
        else
        {
            tower = suitableTowers[Random.Range(0, suitableTowers.Count)];
            return true;
        }
    }

    private void SortSpawnPoints()
    {
        foreach (var turr in spawnPoints)
        {
            switch (turr.GetTurretSpawnType())
            {
                case TowerSpawnPoint.TurretSpawnPointTypes.Front:
                    frontline.Add(turr);
                    break;
                case TowerSpawnPoint.TurretSpawnPointTypes.Mid:
                    midline.Add(turr);
                    break;
                case TowerSpawnPoint.TurretSpawnPointTypes.Back:
                    backline.Add(turr);
                    break;
            }
        }
    }

    private void ChangeDestroyedToEmpty()
    {
        foreach(var spawnpoint in spawnPoints)
            if(spawnpoint.GetTurretSpawnState() == TowerSpawnPoint.TurretSpawnPointStates.Destroyed)
                spawnpoint.StateEmpty();
    }

    private void SpawnAllTurrets()
    {
        while (SpawnTowerProcedure());
    }

    private void SpawnSingleTower(TowerSpawnPoint spawnPoint, Tower towerToSpawn)
    {
        GameObject towerInstance = Instantiate(towerToSpawn.gameObject, spawnPoint.transform.position, spawnPoint.transform.rotation, spawnPoint.transform);
        GameObject hiddenTower;
        Tower towerScript = towerInstance.GetComponent<Tower>();

        switch(towerScript.TowerData.ElementType)
        {
            default:
                hiddenTower = Instantiate(hiddenTowerPrefab[0], spawnPoint.transform.position, spawnPoint.transform.rotation, spawnPoint.transform);
                break;
        }

        towerInstance.GetComponent<Tower>().Spawn(spawnPoint);
        hiddenTowers.Add((towerScript, hiddenTower));

        towerInstance.SetActive(false);

        EnemyBrain.instance.EnemyPayTickets(towerToSpawn.TowerData.Cost);
    }

    private void KillAllTurrets()
    {
        foreach (var turr in spawnedTowers)
            turr.Die();       
        spawnedTowers.Clear();
    }

}
