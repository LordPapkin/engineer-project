using PathCreation;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public int SlimesAliveCounter { get; private set; }
    public List<GameObject> SlimesList { get; private set; }
    public List<GameObject> SlimesAlive { get; private set; }

    private bool slimeSpawningCanceled;

    // TODO: Replace when we know how spawn delay is supposed to work https://app.clickup.com/t/862jg8rvt
    const int SPAWN_DELAY = 1;

    [SerializeField] private Transform slimesSpawnPoint;
    [SerializeField] private PathCreator startPath;


    [Header("Debugging")]
    [SerializeField] private bool repeatTurns;


    private void Start()
    {
        SlimesList = new();
        SlimesAlive = new();
    }

    private void OnDestroy()
    {
        slimeSpawningCanceled = true;
    }

    public void SubscribeToEventSystem()
    {
        TurnEventSystem.instance.Action_BattleStart += OnBattleStart;
    }

    public void AddSlimeToList(GameObject slime)
    {
        SlimesList.Add(slime);
    }

    private void OnBattleStart()
    {
        Debug.Log("Start Slime spawning");
        StartSlimeSpawning(SpawningDoneCallBack, OnAllSlimesDied);
    }

    private void OnAllSlimesDied()
    {
        Debug.Log("All slimes died");
        TurnEventSystem.instance.NextState(TurnState.BattleInProgress, TurnState.BattleFinished);
    }

    private void SpawningDoneCallBack()
    {
        Debug.Log("All Slimes Spawned");
    }

    public void StartSlimeSpawning(Action spawningDoneCallback, Action allSlimesDeadCallback)
    {
        _ = SpawnSlimesTask(spawningDoneCallback, allSlimesDeadCallback);
    }

    public async Task SpawnSlimesTask(Action spawningDoneCallback, Action allSlimesDeadCallback) {
        while (SlimesList.Count > 0) {
            SpawnFirstInList();

            await Task.Delay(SPAWN_DELAY * 1000);

            if (slimeSpawningCanceled)
            {
                slimeSpawningCanceled = false;
                break;
            }
        }

        spawningDoneCallback.Invoke();

        while (SlimesAlive.Count > 0) {
            await Task.Delay(100);
        }

        allSlimesDeadCallback.Invoke();
    }

    public void RemoveFromAliveList(GameObject slime) {
        SlimesAlive.Remove(slime);
        SlimesAliveCounter--;
    }

    public void AddToAliveList(GameObject slime)
    {
        SlimesAlive.Add(slime);
        SlimesAliveCounter++;
    }

    private void SpawnFirstInList()
    {
        var instance = Instantiate(SlimesList[0], slimesSpawnPoint.position, slimesSpawnPoint.rotation);
        instance.GetComponent<Slime>().Spawn(this, startPath);

        SlimesList.RemoveAt(0);
    }
}
