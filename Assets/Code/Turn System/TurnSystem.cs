using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnSystem : MonoBehaviour
{
    public enum State
    {
        SelectingSlimes,
        SpawningSlimes,
        WaitingForTurnEnd
    }

    [SerializeField] private Spawner spawner;

    [Header("Debugging")]
    [SerializeField] private bool startTurn;

    public int CurrentTurn { get; private set; }
    public State CurrentState { get; private set; }


    private void Start()
    {
        CurrentTurn = 0;
        CurrentState = State.SelectingSlimes;
    }

    private void Update() {
        if (startTurn) {
            startTurn = false;
            StartTurn();
        }
    }


    public void StartTurn() {
        if (CurrentState != State.SelectingSlimes) {
            Debug.LogWarning("Cannot start turn because turn system is not in slime selection state");
            return;
        }

        CurrentState = State.SpawningSlimes;

        spawner.StartSlimeSpawning(OnFinishedSpawning, OnAllSlimesDied);
    }

    private void OnFinishedSpawning()
    {
        CurrentState = State.WaitingForTurnEnd;
    }

    private void OnAllSlimesDied() {
        CurrentState = State.SelectingSlimes;
    }
}
