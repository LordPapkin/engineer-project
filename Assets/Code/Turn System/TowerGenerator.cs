using System.Collections.Generic;
using UnityEngine;

// TODO: Actual tower generator https://app.clickup.com/t/862jg8rmz
public class TowerGenerator
{
    private List<Tower> avaiableTowers;

    public TowerGenerator(List<Tower> avaiableTowers)
    {
        this.avaiableTowers = avaiableTowers;
    }

    public List<Tower> GenerateTowers()
    {
        var towers = new List<Tower>();

        for (int i = 0; i < Random.Range(1,4); i++)
        {
            towers.Add(avaiableTowers[0]);
        }

        return towers;
    }
}
