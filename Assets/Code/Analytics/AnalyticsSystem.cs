using System;
using System.Linq;
using System.Text;
using Unity.Services.Analytics;
using Unity.Services.Core;
using UnityEngine;

public class AnalyticsSystem : MonoBehaviour
{
    public static bool isInitialized;

    [SerializeField] private DataCollectionOptions dataCollectionOptions;
    [SerializeField] private TurnEventSystem turnSystem;
    [SerializeField] private Spawner spawner;
    [SerializeField] private ObjectiveManager objectiveManager;
    [SerializeField] private LevelDatastore levelDatastore;

    private async void Start()
    {
        if (!isInitialized)
        {
            Debug.LogWarning("Analytics system wasn't initialized. This is normal if game wasn't started from main menu. Will try to initialize again now");

            await UnityServices.InitializeAsync();
            isInitialized = true;

            if (PlayerPrefs.GetInt(AnalyticsConsts.CONSENT_GIVEN_PREFS) == 1)
                dataCollectionOptions.OptedInHandler();
        }

        turnSystem.Action_BattleStart += HandleBattleStarted;
        objectiveManager.LevelLose += HandleLevelLose;
        objectiveManager.LevelWin += HandleLevelWin;
    }

    private void OnDestroy()
    {
        turnSystem.Action_BattleStart -= HandleBattleStarted;
        objectiveManager.LevelLose -= HandleLevelLose;
        objectiveManager.LevelWin -= HandleLevelWin;
    }

    private void HandleBattleStarted()
    {
        var turnNumber = turnSystem.TurnNumber;
        var levelName = LevelDatastore.CurrentLevelData.sceneName;
        var sentSlimes = string.Join(", ", spawner.SlimesList.Select(x => x.name));
        var resources = BuildCurrentResourcesString();

        AnalyticsEvents.SendTurnStartedEvent(turnNumber, levelName, sentSlimes, resources);
    }

    private void HandleLevelLose()
    {
        var levelName = LevelDatastore.CurrentLevelData.sceneName;
        var resources = BuildCurrentResourcesString();

        AnalyticsEvents.SendLoseEvent(levelName, resources);
    }

    private void HandleLevelWin()
    {
        var turnNumber = turnSystem.TurnNumber;
        var levelName = LevelDatastore.CurrentLevelData.sceneName;
        var resources = BuildCurrentResourcesString();

        AnalyticsEvents.SendWinEvent(turnNumber, levelName, resources);
    }

    private string BuildCurrentResourcesString()
    {
        var builder = new StringBuilder();

        foreach (ResourceType resourceType in Enum.GetValues(typeof(ResourceType)))
        {
            var amount = ResourceManager.Instance.GetResourceAmount(resourceType);
            builder.Append($"{Enum.GetName(typeof(ResourceType), resourceType)}-{amount}, ");
        }

        return builder.ToString();
    }
}
