using Unity.Services.Analytics;
using UnityEngine;
using UnityEngine.UI;

public class DataCollectionOptions : MonoBehaviour
{
    [SerializeField] private Button OptOutButton;
    [SerializeField] private Button OptInButton;
    [SerializeField] private Button RemoveDataButton;

    private void Start()
    {
        OptOutButton.onClick.AddListener(OptedOutHandler);
        OptInButton.onClick.AddListener(OptedInHandler);
        RemoveDataButton.onClick.AddListener(RemoveDataHandler);
    }

    public void OptedOutHandler()
    {
        OptOutButton.gameObject.SetActive(false);
        OptInButton.gameObject.SetActive(true);
        PlayerPrefs.SetInt(AnalyticsConsts.CONSENT_GIVEN_PREFS, 0);

#if ENABLE_CLOUD_SERVICES_ANALYTICS
        AnalyticsService.Instance.StopDataCollection();
#endif
    }

    public void OptedInHandler()
    {
        OptOutButton.gameObject.SetActive(true);
        OptInButton.gameObject.SetActive(false);
        PlayerPrefs.SetInt(AnalyticsConsts.CONSENT_GIVEN_PREFS, 1);

#if ENABLE_CLOUD_SERVICES_ANALYTICS
        AnalyticsService.Instance.StartDataCollection();
#endif
    }

    private void RemoveDataHandler()
    {
#if ENABLE_CLOUD_SERVICES_ANALYTICS
        AnalyticsService.Instance.RequestDataDeletion();
#endif
    }
}
