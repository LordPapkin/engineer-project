using UnityEngine;
using System;

public class Popup : MonoBehaviour
{
    public Action confirmAction;
    public Action closeAction;

    public void Show()
    {
        gameObject.SetActive(true);
    }

    public void Close()
    {
        closeAction.Invoke();
        gameObject.SetActive(false);
    }

    public void Confirm()
    {
        confirmAction.Invoke();
        Close();
    }
}
