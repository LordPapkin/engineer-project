using UnityEngine;

public class QuitEvent : MonoBehaviour
{
    [RuntimeInitializeOnLoadMethod]
    static void RunOnStart()
    {
        Application.quitting += Quit;
    }

    static void Quit()
    {
        var timePlayed = Time.time;
        var loseCount = SessionStatistics.loseCount;
        var winCount = SessionStatistics.winCount;

        AnalyticsEvents.SendGameEndedEvent(timePlayed, loseCount, winCount);
    }
}
