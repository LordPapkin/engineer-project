public class AnalyticsConsts {
    public const string CONSENT_GIVEN_PREFS = "WasAnalyticsConsentGiven";
    public const string CONSENT_POPUP_SHOWN_PREFS = "WasAnalyticsConsentPopupShown";
}