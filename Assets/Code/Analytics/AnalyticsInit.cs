using UnityEngine;
using Unity.Services.Core;

public class AnalyticsInit : MonoBehaviour
{
	[SerializeField] private Popup popup;
	[SerializeField] private DataCollectionOptions dataCollectionOptions;

	async void Start()
	{
		await UnityServices.InitializeAsync();
		AnalyticsSystem.isInitialized = true;

		if (PlayerPrefs.GetInt(AnalyticsConsts.CONSENT_POPUP_SHOWN_PREFS) == 0)
        {
			PlayerPrefs.SetInt(AnalyticsConsts.CONSENT_POPUP_SHOWN_PREFS, 1);
			popup.Show();
			popup.confirmAction += YesHandler;
			popup.closeAction += CloseHandler;
		}

		if (PlayerPrefs.GetInt(AnalyticsConsts.CONSENT_GIVEN_PREFS) == 1)
        {
			dataCollectionOptions.OptedInHandler();
		}
	}

    void YesHandler()
	{
		dataCollectionOptions.OptedInHandler();
		PlayerPrefs.SetInt(AnalyticsConsts.CONSENT_GIVEN_PREFS, 1);
	}

	void CloseHandler()
    {
		popup.confirmAction -= YesHandler;
		popup.closeAction -= CloseHandler;
	}
}
