using System.Collections.Generic;
using Unity.Services.Analytics;
using UnityEngine;

public static class AnalyticsEvents
{
    public static void SendTurnStartedEvent(int turnNumber, string levelName, string sentSlimes, string resources)
    {
        // Event "turnStarted"
        var parameters = new Dictionary<string, object>()
        {
            { "turnNumber",  turnNumber}, // int
            { "levelName", levelName}, // string
            { "sentSlimes",  sentSlimes}, // string
            { "resources",  resources} // string
        };

        TrySendEvent("turnStarted", parameters);
    }

    public static void SendWinEvent(int turnNumber, string levelName, string resources)
    {
        // Event "levelWin"
        var parameters = new Dictionary<string, object>()
        {
            { "turnNumber",  turnNumber}, // int
            { "levelName", levelName}, // string
            { "resources",  resources} // string
        };

        TrySendEvent("levelWin", parameters);
    }

    public static void SendLoseEvent(string levelName, string resources)
    {
        // Event "levelLose"
        var parameters = new Dictionary<string, object>()
        {
            { "levelName", levelName}, // string
            { "resources",  resources} // string
        };

        TrySendEvent("levelLose", parameters);
    }

    public static void SendGameEndedEvent(float timePlayed, int loseCount, int winCount)
    {
        // Event "customGameEnded"
        var parameters = new Dictionary<string, object>()
        {
            { "timePlayed", timePlayed}, // float
            { "loseCount",  loseCount}, // int
            { "winCount",  winCount} // int
        };

        TrySendEvent("customGameEnded", parameters);
    }


    static void TrySendEvent(string eventName, Dictionary<string, object> parameters)
    {
#if ENABLE_CLOUD_SERVICES_ANALYTICS
        if (PlayerPrefs.GetInt(AnalyticsConsts.CONSENT_GIVEN_PREFS) == 0)
            Debug.LogWarning("Can't send event because you didn't consent for data collection. Go to options and opt in for data collection");

        AnalyticsService.Instance.CustomData(eventName, parameters);
#endif
    }
}
