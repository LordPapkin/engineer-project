using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ObjectiveManager : MonoBehaviour
{
    public Action LevelWin;
    public Action LevelLose;

    [SerializeField] private ObjectiveProgressBar healthBarTemplate;
    [SerializeField] private Transform healthBarsParent;
    [SerializeField] private int levelToLoadID;
    [SerializeField] private EndDoor[] endDoors;

    private Dictionary<EndDoor, ObjectiveProgressBar> doorsAndHealthBarsDict;

    public static ObjectiveManager Instance => _instance;
    private static ObjectiveManager _instance;


    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }

        doorsAndHealthBarsDict = new Dictionary<EndDoor, ObjectiveProgressBar>();
    }
    
    private void Start()
    {
        TurnEventSystem.instance.Action_SpawnTowers += UpdateTurnCounter;
        foreach (var endDoor in endDoors)
        {
            var healthBar = Instantiate(healthBarTemplate, healthBarsParent);
            healthBar.SetUpSlider(LevelDatastore.CurrentLevelData.slimesPassedToWin,
                LevelDatastore.CurrentLevelData.slimesPassedToWin);
            healthBar.text.SetText(endDoor.gameObject.name);
            doorsAndHealthBarsDict.Add(endDoor, healthBar);
            healthBar.gameObject.SetActive(true);
        }
    }

    private void OnDestroy()
    {
        TurnEventSystem.instance.Action_SpawnTowers -= UpdateTurnCounter;
    }
    
    public void HandleSlimePassed(int damage, EndDoor endDoor)
    {
        doorsAndHealthBarsDict[endDoor].TakeHealth(damage);
        CheckVictoryCondition();
    }

    private void UpdateTurnCounter()
    {
        var maxTurns = LevelDatastore.CurrentLevelData.maxTurns;
        var currentTurn = TurnEventSystem.instance.TurnNumber;

        if (currentTurn > maxTurns)
        {
            Debug.Log("LOST");
            SessionStatistics.loseCount++;
            LevelLose.Invoke();
            SceneManager.LoadScene("Scenes/ScenesForBuild/Menus/LostScene");
        }
    }

    void CheckVictoryCondition()
    {
        int objectivesDone = 0;
        foreach (var objective in doorsAndHealthBarsDict)
        {
            if (objective.Value.GetSliderValue() == 0)
                objectivesDone++;
        }

        if (objectivesDone != endDoors.Length)
            return;

        Debug.Log("WIN!!!");
        SessionStatistics.winCount++;
        LevelWin.Invoke();
        PlayerPrefs.SetInt("LevelID", levelToLoadID);
        SceneManager.LoadScene("Scenes/ScenesForBuild/Menus/WinScene");
    }
}
