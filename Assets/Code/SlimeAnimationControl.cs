using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

public class SlimeAnimationControl : MonoBehaviour
{
    [SerializeField] private Animator SlimeAnimator;

    public void Idle()
    {
        
    }
    public void Move()
    {
        SlimeAnimator.SetBool("Move", true);
    }

    public void Attack()
    {
        SlimeAnimator.SetTrigger("Basic_Attack");
    }
    public void SpecialAttack()
    {
        SlimeAnimator.SetTrigger("Special_Attack");
    }
    public void Spawn()
    {
        SlimeAnimator.SetTrigger("Spawn");
    }

    public void Gather()
    {
        SlimeAnimator.SetTrigger("Gather");
    }

    void Start()
    {
    }
}
