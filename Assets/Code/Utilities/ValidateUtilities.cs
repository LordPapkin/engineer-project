using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Object = UnityEngine.Object;


public static class ValidateUtilities
{
    public static bool NullCheckField<T>(Object objectToCheck, string fieldName, T fieldValue, bool isError)
    {
        if (fieldValue == null)
        {
            if (isError)
            {
                Debug.LogError($"{fieldName} has no value in object {objectToCheck.name}");
                return false;
            }
            Debug.LogWarning($"{fieldName} has no value in object {objectToCheck.name}");
        }

        return true;
    }
    
    public static bool ValidateStringField(Object objectToCheck, string fieldName, string fieldValue)
    {
        if (String.IsNullOrEmpty(fieldValue))
        {
            Debug.LogError($"{fieldName} is empty and must contain value in object {objectToCheck.name}");
            return false;
        }
        return true;
    }

    public static bool ValidateEnumerableField(Object objectToCheck, string fieldName, IEnumerable fieldValue)
    {
        int count = 0;
        foreach (var item in fieldValue)
        {
            if (item == null)
            {
                Debug.LogError($"{fieldName} has null values in object {objectToCheck.name}");
                return false;
            }
            count++;
        }

        if (count == 0)
        {
            Debug.LogError($"{fieldName} has no values in object {objectToCheck.name}");
            return false;
        }
        
        return true;
    }
    
    public static bool ValidateResourceAmountField(Object objectToCheck, string fieldName, ResourceAmount fieldValue)
    {
        if (fieldValue.Amount < 0)
        {
            Debug.LogWarning($"{nameof(fieldValue.Amount)} has negative value in object {objectToCheck.name}");
        }
        
        return true;
    }
    
    public static bool ValidateResourceAmountField(Object objectToCheck, string fieldName, IEnumerable<ResourceAmount> fieldValue)
    {
        int count = 0;
        foreach (var item in fieldValue)
        {
            if (item.Amount < 0)
            {
                Debug.LogWarning($"{nameof(item.Amount)} has negative value in object {objectToCheck.name}");
            }
            else
            {
                count++;
            }
        }

        if (count == 0)
        {
            Debug.LogError($"{fieldName} has no values in object {objectToCheck.name}");
            return false;
        }
        
        return true;
    }

}
