using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;
using UnityEngine.SceneManagement;

public class TurnCounter : MonoBehaviour
{
    private TMP_Text counterText;

    private void Start()
    {
        counterText = GetComponent<TMP_Text>();
        TurnEventSystem.instance.Action_SpawnTowers += UpdateCounter;
    }

    private void OnDestroy()
    {
        TurnEventSystem.instance.Action_SpawnTowers -= UpdateCounter;
    }

    private void UpdateCounter()
    {
        var maxTurns = LevelDatastore.CurrentLevelData.maxTurns;
        var currentTurn = TurnEventSystem.instance.TurnNumber;
        counterText.text = $"Turn {currentTurn}/{maxTurns}";
    }
}
