using UnityEngine;
using TMPro;

public class LevelHUD : MonoBehaviour
{
    [SerializeField] private TMP_Text turnCounterText;
    [SerializeField] private TMP_Text moneyCounterText;
    [SerializeField] private TMP_Text slimeCounterText;
    [SerializeField] private TMP_Text turretCounterText;

    private int _turnCounter;
    public int TurnCounter {
        get => _turnCounter;
        set {
            _turnCounter = value;
            turnCounterText.text = value.ToString();
        }
    }

    private int _moneyCounter;
    public int MoneyCounter {
        get => _moneyCounter;
        set {
            _moneyCounter = value;
            moneyCounterText.text = value.ToString();
        }
    }

    private int _slimeCounter;
    public int SlimeCounter {
        get => _slimeCounter;
        set {
            _slimeCounter = value;
            slimeCounterText.text = value.ToString();
        }
    }

    private int _turretCounter;
    public int TurretCounter {
        get => _turretCounter;
        set {
            _turretCounter = value;
            turretCounterText.text = value.ToString();
        }
    }
}
