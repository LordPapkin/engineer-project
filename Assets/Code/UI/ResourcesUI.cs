using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ResourcesUI : MonoBehaviour
{
    [Serializable]
    private class ResourceCounter
    {
        public TextMeshProUGUI resourcesCounter;
        public TextMeshProUGUI resourcesUpdateCounter;
        public float timeToHideUpdateCounter;
    }

    [SerializeField] private ResourceCounter[] resourcesCounters;
    private Dictionary<ResourceType, ResourceCounter> resourcesCounterDictionary;

    private const float UPDATE_COUNTER_VISIBLE_SECONDS = 0.666f;

    private void OnEnable()
    {
        ResourceManager.Instance.onResourceAmountChanged += UpdateCounters;
    }

    private void OnDisable()
    {
        ResourceManager.Instance.onResourceAmountChanged -= UpdateCounters;
    }

    private void Awake()
    {
        resourcesCounterDictionary = new Dictionary<ResourceType, ResourceCounter>();
        ResourceType[] resources = (ResourceType[])Enum.GetValues(typeof(ResourceType));
        for (var i = 0; i < resources.Length; i++)
        {
            resourcesCounters[i].resourcesUpdateCounter.gameObject.SetActive(false);
            resourcesCounterDictionary[resources[i]] = resourcesCounters[i];
        }
    }

    private void Update()
    {
        foreach (var resource in resourcesCounters)
        {
            resource.timeToHideUpdateCounter -= Time.deltaTime;

            if (resource.timeToHideUpdateCounter <= 0)
                resource.resourcesUpdateCounter.gameObject.SetActive(false);
        }
    }

    private void UpdateCounters(ResourceOperation resourceOp)
    {
        if (resourceOp == null)
        {
            UpdateAll();
            return;
        }

        var type = resourceOp.resourceType;
        var counter = resourcesCounterDictionary[type];

        counter.resourcesCounter.SetText(ResourceManager.Instance.GetResourceAmount(type).ToString());
        counter.resourcesUpdateCounter.SetText((resourceOp.signedAmount > 0 ? "+" : "") + resourceOp.signedAmount.ToString());
        counter.resourcesUpdateCounter.color = resourceOp.signedAmount > 0 ? Color.green : Color.red;
        counter.timeToHideUpdateCounter = UPDATE_COUNTER_VISIBLE_SECONDS;

        counter.resourcesUpdateCounter.gameObject.SetActive(false);
        counter.resourcesUpdateCounter.gameObject.SetActive(true);
    }

    private void UpdateAll()
    {
        foreach (ResourceType resource in Enum.GetValues(typeof(ResourceType)))
        {
            var amountString = ResourceManager.Instance.GetResourceAmount(resource).ToString();
            resourcesCounterDictionary[resource].resourcesCounter.SetText(amountString);
        }
    }
}
