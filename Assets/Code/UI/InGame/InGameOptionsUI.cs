using System;
using UnityEngine;
using UnityEngine.InputSystem;

public class InGameOptionsUI : MonoBehaviour
{
    [SerializeField] private InputReader inputReader;
    [SerializeField] private GameObject optionsPanel;

    private void Start()
    {
        inputReader.OpenMenu += HandleOpenMenu;
    }

    private void HandleOpenMenu(InputAction.CallbackContext obj)
    {
        optionsPanel.SetActive(!optionsPanel.activeSelf);
    }
}
