using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    [SerializeField] private Slider healthSlider;
    [SerializeField] private Transform unit;
    [SerializeField] private float yOffset = 1.0f;

    void Update()
    {
        if (unit != null)
        {
            transform.position = unit.position + Vector3.up * yOffset;
            transform.forward = Camera.main.transform.forward;
        }
    }

    public void UpdateHealthBar(float currentHealthRatio)
    {
        healthSlider.value = currentHealthRatio;
    }
}
