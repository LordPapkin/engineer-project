using UnityEngine;

public class ResourceUpdateCounter : MonoBehaviour
{
    private float currentScale;

    private void OnEnable()
    {
        currentScale = 0;
        transform.localScale = new Vector3(currentScale, currentScale, currentScale);
    }

    private void Update()
    {
        currentScale = Mathf.Clamp01(currentScale + Time.deltaTime * 10);
        transform.localScale = new Vector3(currentScale, currentScale, currentScale);
    }
}
