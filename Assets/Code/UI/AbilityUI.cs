using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class AbilityUI : MonoBehaviour
{
    [SerializeField] private InputReader inputReader;
    [SerializeField] private Camera mainCamera;
    [SerializeField] private Slider manaSlider;
    [SerializeField] private EffectData healEffectData;
    [SerializeField] private EffectData speedUpEffectData;
    [SerializeField] private Button healButton;
    [SerializeField] private Button speedUpButton;
    [SerializeField] private Outline healOutline;
    [SerializeField] private Outline speedUpOutline;
    private EffectType? currentEffect;

    private void Awake()
    {
        inputReader.RightMouseClick += HandleRightMouseClick;
        inputReader.LeftMouseClick += HandleLeftMouseClick;
        healButton.onClick.AddListener(AddHealingEffect);
        speedUpButton.onClick.AddListener(AddSpeedUpEffect);
    }

    private void Start()
    {
        TurnEventSystem.instance.Action_BattleStart += HandleBattleStarted;
        TurnEventSystem.instance.Action_BattleFinished += HandleBattleFinished;
    }

    private void OnDestroy()
    {
        inputReader.RightMouseClick -= HandleRightMouseClick;
        inputReader.LeftMouseClick -= HandleLeftMouseClick;
        healButton.onClick.RemoveAllListeners();
        speedUpButton.onClick.RemoveAllListeners();
    }
    
    private void HandleRightMouseClick(InputAction.CallbackContext obj)
    {
        if (currentEffect == EffectType.Heal)
        {
            healOutline.enabled = false;
        }
        else
        {
            speedUpOutline.enabled = false;
        }

        manaSlider.value += 20;
        currentEffect = null;
    }
    
    private void HandleLeftMouseClick(InputAction.CallbackContext obj)
    {
        if (currentEffect == null) 
            return;
        
        var ray = mainCamera.ScreenPointToRay(inputReader.MousePosition());

        if (!Physics.Raycast(ray, out var hit))
            return;
        
        var colliders = Physics.OverlapSphere(hit.point, 5f);
        foreach (var collider in colliders)
        {
            if (!collider.gameObject.CompareTag("Slime"))
                continue;
                
            if (currentEffect == EffectType.Heal)
            {
                Effect effect = new HealEffect(healEffectData.EffectValue, healEffectData.EffectDamageFrequency,
                    healEffectData.EffectDuration);
                collider.GetComponent<Slime>().TakeEffect(effect, Particles.Heal);
                healOutline.enabled = false;
            }
            else
            {
                Effect effect = new SpeedUpEffect(speedUpEffectData.EffectDuration);
                collider.GetComponent<Slime>().TakeEffect(effect, Particles.SpeedUp);
                speedUpOutline.enabled = false;
            }
        }
        currentEffect = null;
    }

    private void HandleBattleFinished()
    {
        if (currentEffect != null)
        {
            currentEffect = null;
            manaSlider.value += 20;
        }
        manaSlider.value += 40;
        healOutline.enabled = false;
        speedUpOutline.enabled = false;
        ToggleButtonsInteractable(false);
    }

    private void HandleBattleStarted()
    {
        ToggleButtonsInteractable(true);
    }
    
    private void AddHealingEffect()
    {
        AddEffect(EffectType.Heal);
    }

    private void AddSpeedUpEffect()
    {
        AddEffect(EffectType.SpeedUp);
    }

    private void AddEffect(EffectType effectType)
    {
        if(manaSlider.value < 20)
            return;
        currentEffect = effectType;
        if (currentEffect == EffectType.Heal)
        {
            healOutline.enabled = true;
        }
        else
        {
            speedUpOutline.enabled = true;
        }
        manaSlider.value -= 20;

        if (manaSlider.value < 20)
            ToggleButtonsInteractable(false);
    }

    void ToggleButtonsInteractable(bool isInteractable)
    {
        healButton.interactable = isInteractable;
        speedUpButton.interactable = isInteractable;
    }
}
