using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UnitsPanelUI : MonoBehaviour
{
   [SerializeField] private Sprite[] resourcesIcons;
   [SerializeField] private GameObject unitsPanel;
   [SerializeField] private Button startButton;
   [SerializeField] private Spawner slimeSpawner;
   [SerializeField] private UnitQueue unitQueue;
   [SerializeField] private Transform unitQueueTransform;

   private void Start()
   {
      TurnEventSystem.instance.Action_SlimeAndPathSelecting += OnActionSlimeAndPathSelecting;
   }

   private void OnActionSlimeAndPathSelecting()
   {
      unitsPanel.SetActive(true);
      startButton.gameObject.SetActive(true);
      startButton.interactable = false;
   }

   public void AddUnitToList(GameObject slime, Sprite slimeSprite)
   {
      unitQueue.AddUnitToQueue(slimeSprite);
      slimeSpawner.AddSlimeToList(slime);
      startButton.interactable = true;
      unitQueueTransform.gameObject.SetActive(true);
   }
   
   public void Clear()
   {
      unitQueue.Clear();
      foreach (var slime in slimeSpawner.SlimesList)
      {
         var slimeScript = slime.GetComponent<Slime>();
         ResourceManager.Instance.AddResources(slimeScript.SlimeData.ResourceAmountArrayData.ResourceAmountData);
      }
      slimeSpawner.SlimesList.Clear();
   }

   public void NextTurnStage()
   {
      unitQueue.Clear();
      unitsPanel.SetActive(false);
      startButton.gameObject.SetActive(false);
      TurnEventSystem.instance.MoveToNext();
   }
   
   public Sprite GetIcon(int spriteIndex)
   {
      return resourcesIcons[spriteIndex];
   }

   private void OnValidate()
   {
      ValidateUtilities.NullCheckField(this, nameof(resourcesIcons), resourcesIcons, true);
      ValidateUtilities.NullCheckField(this, nameof(unitsPanel), unitsPanel, true);
      ValidateUtilities.NullCheckField(this, nameof(startButton), startButton, true);
   }
}
