using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UnitResourceCostUI : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI text;
    [SerializeField] private Image icon;

    public void SetUp(int amount, Sprite sprite)
    {
        text.SetText(amount.ToString());
        icon.sprite = sprite;
    }

    private void OnValidate()
    {
        ValidateUtilities.NullCheckField(this, nameof(icon), icon, true);
        ValidateUtilities.NullCheckField(this, nameof(text), text, true);
    }
}
