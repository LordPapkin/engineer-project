using System;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;
using UnityEngine.SceneManagement;

public class AfterLevelUI : MonoBehaviour
{
    private IDisposable disposable;
    
    private void OnEnable()
    {
       disposable = InputSystem.onAnyButtonPress.CallOnce(HandleAnyButtonPressed);
    }

    private void OnDisable()
    {
        disposable.Dispose();
    }

    private void HandleAnyButtonPressed(InputControl obj)
    {
        SceneManager.LoadScene("Scenes/ScenesForBuild/Menus/LevelSelectionScene3D");
    }
}
