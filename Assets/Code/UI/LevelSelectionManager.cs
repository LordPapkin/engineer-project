using UnityEngine;
using UnityEngine.UI;

public class LevelSelectionManager : MonoBehaviour
{
    [SerializeField] private Button[] levelsButtons;
    [SerializeField] private GameObject[] regularTurret;
    [SerializeField] private GameObject[] destroyedTurret;
    [SerializeField] private ParticleSystem[] fireParticles;

    private int currentIndex;

    private void Awake()
    {
        currentIndex = PlayerPrefs.GetInt("LevelID", 0);
        
        for (int i = 0; i < levelsButtons.Length; i++)
        {
            if (i == currentIndex)
            {
                levelsButtons[i].interactable = true;
                regularTurret[i].SetActive(true);
                destroyedTurret[i].SetActive(false);
                fireParticles[i].gameObject.SetActive(false);
            }
            else if (i > currentIndex)
            {
                levelsButtons[i].interactable = false;
                regularTurret[i].SetActive(true);
                destroyedTurret[i].SetActive(false);
                fireParticles[i].gameObject.SetActive(false);
            }
            else
            {
                levelsButtons[i].enabled = false;
                regularTurret[i].SetActive(false);
                destroyedTurret[i].SetActive(true);
                fireParticles[i].gameObject.SetActive(true);
                fireParticles[i].Play();
            }
        }
    }
}