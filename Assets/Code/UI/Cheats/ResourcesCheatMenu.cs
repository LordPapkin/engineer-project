using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System;

public class ResourcesCheatMenu : MonoBehaviour
{
    [SerializeField] private TMP_Dropdown resourceDropdown;
    [SerializeField] private TMP_InputField amountInput;
    [SerializeField] private Button addButton;
    [SerializeField] private Button removeButton;

    private ResourceManager resourcesManager;

    private void Start() {
        resourcesManager = ResourceManager.Instance;
        List<TMP_Dropdown.OptionData> options = new List<TMP_Dropdown.OptionData>();

        foreach (string resourceType in Enum.GetNames(typeof(ResourceType))) {
            TMP_Dropdown.OptionData option = new(resourceType);
            options.Add(option);
        }

        resourceDropdown.options = options;


        addButton.onClick.AddListener(() => 
        {
            resourcesManager.AddResources(new ResourceAmount((ResourceType)resourceDropdown.value, int.Parse(amountInput.text)));
        });

        removeButton.onClick.AddListener(() => {
            resourcesManager.TrySpendResources(new ResourceAmount((ResourceType)resourceDropdown.value, int.Parse(amountInput.text)));
        });
    }
}
