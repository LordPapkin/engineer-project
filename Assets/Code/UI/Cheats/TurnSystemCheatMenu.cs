using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TurnSystemCheatMenu : MonoBehaviour
{
    [SerializeField] private Button nextStateButton;

    private void Start() {
        nextStateButton.onClick.AddListener(() => {
            TurnEventSystem.instance.MoveToNext();
        });
    }
}
