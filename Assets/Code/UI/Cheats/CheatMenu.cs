using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class CheatMenu : MonoBehaviour
{
    private Canvas canvas;

    private void Start() {
        canvas = GetComponent<Canvas>();
        canvas.enabled = false;
    }

    private void Update() {
#if DEVELOPMENT_BUILD || UNITY_EDITOR
        if (Keyboard.current[Key.Q].wasPressedThisFrame) {
            canvas.enabled = canvas.enabled ? false : true;
        }
#endif
    }
}
