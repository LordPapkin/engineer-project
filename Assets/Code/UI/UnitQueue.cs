using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UnitQueue : MonoBehaviour
{
    [SerializeField] private Transform queueTransform;
    [SerializeField] private GameObject template;
    [SerializeField] private Button resetButton;

    List<GameObject> gameObjectList= new List<GameObject>();

    public void AddUnitToQueue(Sprite unitSprite)
    {
        var addedGameObject = Instantiate(template, queueTransform);
        addedGameObject.GetComponent<UnitQueueTemplate>().SetImage(unitSprite);
        addedGameObject.SetActive(true);
        gameObjectList.Add(addedGameObject);
        resetButton.gameObject.SetActive(true);
    }

    public void Clear()
    {
        queueTransform.gameObject.SetActive(false);
        resetButton.gameObject.SetActive(false);
        foreach (var unit in gameObjectList)
        {
            Destroy(unit);
        }
        gameObjectList.Clear();
    }
}
