using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UnitUI : MonoBehaviour
{
    [SerializeField] private UnitsPanelUI unitPanelUI;
    [SerializeField] private RectTransform costVisual;
    [SerializeField] private RectTransform parentForTemplate;
    [SerializeField] private GameObject template;
    [SerializeField] private SlimeData slimeData;
    [SerializeField] private Image unitImage;
    [SerializeField] private TextMeshProUGUI availableUnitsNumberText;

    private Coroutine closingCoroutine;

    private void Awake()
    {
        foreach (var resource in slimeData.ResourceAmountArrayData.ResourceAmountData)
        {
            var templateClone = Instantiate(template, parentForTemplate.transform).GetComponent<UnitResourceCostUI>();
            templateClone.SetUp(resource.Amount, unitPanelUI.GetIcon((int)resource.ResourceType));
        }
        costVisual.sizeDelta = new Vector2(110f, slimeData.ResourceAmountArrayData.ResourceAmountData.Length * 50);
        template.SetActive(false);
    }

    private void Start()
    {
        ResourceManager.Instance.onResourceAmountChanged += HandleResourceAmountChanged;
        HandleResourceAmountChanged(null);
    }

    public void Show()
    {
        costVisual.gameObject.SetActive(true);
        if (closingCoroutine != null)
        {
            StopCoroutine(closingCoroutine);
        }
    }

    public  void Hide(float dur)
    {
        closingCoroutine = StartCoroutine(HideCor(dur));
    }

    public void BoughtUnit()
    {
        if (ResourceManager.Instance.TrySpendResources(slimeData.ResourceAmountArrayData.ResourceAmountData))
        {
            unitPanelUI.AddUnitToList(slimeData.SlimePrefab, unitImage.sprite);
        }
        else
        {
            UpdateAvailableUnits(slimeData.ResourceAmountArrayData.ResourceAmountData);
        }
    }
    
    private IEnumerator HideCor(float dur)
    {
        yield return new WaitForSeconds(dur);
        costVisual.gameObject.SetActive(false);
        closingCoroutine = null;
    }

    private void HandleResourceAmountChanged(ResourceOperation resourceOp)
    {
        UpdateAvailableUnits(slimeData.ResourceAmountArrayData.ResourceAmountData);
    }
    
    private void UpdateAvailableUnits(ResourceAmount[] resourcesAmounts)
    {
        int availableUnitsNumber = ResourceManager.Instance.GetAvailableUnitsNumber(resourcesAmounts);
        availableUnitsNumberText.SetText(availableUnitsNumber.ToString());
    }

    private void OnValidate()
    {
        ValidateUtilities.NullCheckField(this, nameof(costVisual), costVisual, true);
        ValidateUtilities.NullCheckField(this, nameof(parentForTemplate), parentForTemplate, true);
        ValidateUtilities.NullCheckField(this, nameof(template), template, true);
        ValidateUtilities.NullCheckField(this, nameof(slimeData), slimeData, true);
    }
}
