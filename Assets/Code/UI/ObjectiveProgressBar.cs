using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ObjectiveProgressBar : MonoBehaviour
{
    [SerializeField] Slider sliderBar;
    public TextMeshProUGUI text;

    public void TakeHealth(int damage)
    {
        sliderBar.value -= damage;
    }

    public void SetUpSlider(int health, int max)
    {
        sliderBar.maxValue = max;
        sliderBar.value = health;
    }

    public int GetSliderValue()
    {
        return (int)sliderBar.value;
    }
}
