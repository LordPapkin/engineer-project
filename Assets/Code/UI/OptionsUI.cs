using UnityEngine;
using UnityEngine.UI;

public class OptionsUI : MonoBehaviour
{
    [SerializeField] private Color basicColor;
    [SerializeField] private Color highlightColor;
    
    public void HighlightButton(Image button)
    {
        button.color = highlightColor;
    }

    public void UnHighlightButton(Image button)
    {
        button.color = basicColor;
    }
}
