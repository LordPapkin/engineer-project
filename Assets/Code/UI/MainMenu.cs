using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class MainMenu : MonoBehaviour
{
    [SerializeField] private Button continueButton;
    [SerializeField] private Button newGameButton;
    [SerializeField] private Button optionButton;
    [SerializeField] private Button quitGameButton;
    [SerializeField] private GameObject optionsPanel;

    private void Awake()
    {
        if (PlayerPrefs.GetInt("LevelID", 0) <= 0)
            continueButton.interactable = false;
        
        continueButton.onClick.AddListener(ButtonContinue);
        newGameButton.onClick.AddListener(ButtonNewGame);
        optionButton.onClick.AddListener(ButtonOptions);
        quitGameButton.onClick.AddListener(ButtonQuit);
    }

    private void OnDestroy()
    {
        continueButton.onClick.RemoveAllListeners();
        newGameButton.onClick.RemoveAllListeners();
        optionButton.onClick.RemoveAllListeners();
        quitGameButton.onClick.RemoveAllListeners();
    }

    private void ButtonContinue()
    {
        LoadLevelSelectionScene();
    }

    private void ButtonNewGame()
    {
        PlayerPrefs.SetInt("LevelID", 0);
        LoadLevelSelectionScene();
    }
    
    private void ButtonOptions()
    {
        OpenMenu();
    }
    
    private void ButtonQuit()
    {
        QuitGame();
    }

    private void LoadLevelSelectionScene()
    {
        SceneManager.LoadScene("Scenes/ScenesForBuild/Menus/LevelSelectionScene3D");
    }

    private void OpenMenu()
    {
        //buttons.gameObject.SetActive(!buttons.activeSelf);
        optionsPanel.gameObject.SetActive(!optionsPanel.activeSelf);
    }

    private void QuitGame()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#endif
        Application.Quit();
    }
}
