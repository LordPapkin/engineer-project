using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleManager : MonoBehaviour
{
    public static ParticleManager Instance { get; private set; }

    private Dictionary<Particles, ParticleSystem> particleSystemsDict;
    private Dictionary<Particles, Queue<ParticleSystem>> particlesQueueDict;

    private void Awake()
    {
        if (Instance == null) 
        {
            Instance = this;
        }
        
        particlesQueueDict = new Dictionary<Particles, Queue<ParticleSystem>>();
        LoadParticles();
    }

    public void PlayParticlesOneShot(Particles particleType, Vector3 position)
    {
        if (particlesQueueDict[particleType].Count > 0)
        {
            var particles = particlesQueueDict[particleType].Dequeue();
            particles.transform.position = position;
            particles.Play();
            StartCoroutine(HandleParticleLifeCor(particles, particleType));
        }
        else
        {
            var particles = Instantiate(particleSystemsDict[particleType], position, Quaternion.identity, transform);
            particles.Play();
            StartCoroutine(HandleParticleLifeCor(particles, particleType));
        }
    }

    public void PlayParticlesOneShot(Particles particleType, Vector3 position, Material material)
    {
        if (particlesQueueDict[particleType].Count > 0)
        {
            var particles = particlesQueueDict[particleType].Dequeue();
            particles.transform.position = position;
            particles.GetComponent<SlimeDeathParticleMaterial>().SetMaterial(material);
            particles.Play();
            StartCoroutine(HandleParticleLifeCor(particles, particleType));
        }
        else
        {
            var particles = Instantiate(particleSystemsDict[particleType], position, Quaternion.identity, transform);
            particles.GetComponent<SlimeDeathParticleMaterial>().SetMaterial(material);
            particles.Play();
            StartCoroutine(HandleParticleLifeCor(particles, particleType));
        }
    }

    public ParticleSystem GetParticleSystem(Particles particleType, Vector3 position)
    {
        if (particlesQueueDict[particleType].Count > 0)
        {
            var particle = particlesQueueDict[particleType].Dequeue();
            particle.transform.position = position;
            return particle;
        }
        return Instantiate(particleSystemsDict[particleType], position, Quaternion.identity);
    }

    public void ReturnParticleSystem(ParticleSystem particles, Particles particleType)
    {
        particles.transform.SetParent(transform);
        particlesQueueDict[particleType].Enqueue(particles);
    }

    private IEnumerator HandleParticleLifeCor(ParticleSystem particles, Particles particleType)
    {
        var main = particles.main;
        yield return new WaitForSeconds(main.duration + main.startLifetime.constantMax);
        particles.Stop();
        particlesQueueDict[particleType].Enqueue(particles);
    }

    private void LoadParticles()
    {
        particleSystemsDict = new Dictionary<Particles, ParticleSystem>();
        foreach (Particles particle in Enum.GetValues(typeof(Particles)))
        {
            particleSystemsDict[particle] = Resources.Load<ParticleSystem>(particle.ToString());
            particlesQueueDict[particle] = new Queue<ParticleSystem>();
        }
    } 
}

public enum Particles
{
    ExplosionSmall,
    Explosion,
    Heal,
    Fire,
    FireSmall,
    SlowDown,
    SpeedUp,
    SlimeDeath
}

public struct ParticlesStruct
{
    public Particles particleType;
    public ParticleSystem particleSystem;

    public ParticlesStruct(Particles particles, ParticleSystem effectsParticles)
    {
        particleType = particles;
        particleSystem = effectsParticles;
    }
}
