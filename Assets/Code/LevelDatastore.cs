using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelDatastore : MonoBehaviour
{
    public static LevelData CurrentLevelData { get; private set; }

    [SerializeField] private LevelData levelData;

    private void Awake()
    {
        CurrentLevelData = levelData;
    }

    private void OnDestroy()
    {
        CurrentLevelData = null;
    }
}
