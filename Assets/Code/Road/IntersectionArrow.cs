using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using PathCreation;

public class IntersectionArrow : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
{
    [SerializeField] private Color hoverColor;
    [SerializeField] private Color selectedColor;
    private Color startColor;

    public PathCreator Path { get; set; }
    public Intersection Intersection { get; set; }
    public IntersectionUI IntersectionUI { get; set; }

    private Image image;
    private bool selected;

    private void Awake()
    {
        image = GetComponent<Image>();
        startColor = image.color;
        image.Rebuild(CanvasUpdate.Layout);
        image.RecalculateClipping();
        image.RecalculateMasking();
        image.SetRaycastDirty();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (!selected)
            image.color = hoverColor;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (!selected)
            image.color = startColor;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        Select();
        Intersection.ChosenPath = Path;

        foreach (var arrow in IntersectionUI.arrows)
        {
            if (arrow != this)
                arrow.Deselect();
        }
    }

    public void Select()
    {
        selected = true;
        image.color = selectedColor;
    }

    public void Deselect()
    {
        selected = false;
        image.color = startColor;
    }
}
