using UnityEngine;

public class EndDoor : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.TryGetComponent(out Slime slime))
        {
            slime.DieOnDoors();
            ObjectiveManager.Instance.HandleSlimePassed(slime.SlimeData.ObjectiveDamage, this);
        }
    }
}
