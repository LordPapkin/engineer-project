﻿using UnityEngine;
using PathCreation;
using System.Collections;

public class PathFollower : MonoBehaviour
{
    const float PATH_CHAGNE_TIME_OFFSET = 0.2f;

    public PathCreator CurrentPath { get; set; }
    public float DistanceTravelled { get; private set; }

    [SerializeField] EndOfPathInstruction endOfPathInstruction;
    [SerializeField] float speed = 1;


    void Start() 
    {
        if (CurrentPath != null)
        {
            CurrentPath.pathUpdated += OnPathChanged;
        }
    }

    void Update()
    {
        FollowPath();
    }

    private void OnDestroy()
    {
        CurrentPath.pathUpdated -= OnPathChanged;
    }

    public void SetDistanceTraveled(float distance)
    {
        DistanceTravelled = distance;
    }

    public void SetSpeed(float speed)
    {
        this.speed = speed;
    }

    public void EnterIntersection(Intersection intersection)
    {
        float timeToSwitchPath = (0.5f + PATH_CHAGNE_TIME_OFFSET) / speed;

        StartCoroutine(SwitchPath());

        IEnumerator SwitchPath()
        {
            yield return new WaitForSeconds(timeToSwitchPath);
            SetPath(intersection.ChosenPath);
        }
    }

    void FollowPath()
    {
        if (CurrentPath != null)
        {
            DistanceTravelled += speed * Time.deltaTime;
            transform.position = CurrentPath.path.GetPointAtDistance(DistanceTravelled, endOfPathInstruction);

            Quaternion targetRotation = CurrentPath.path.GetRotationAtDistance(DistanceTravelled, endOfPathInstruction) * Quaternion.Euler(0, 0, 90);
            transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, speed);
        }
    }

    void SetPath(PathCreator path)
    {
        CurrentPath.pathUpdated -= OnPathChanged;
        CurrentPath = path;
        CurrentPath.pathUpdated += OnPathChanged;
        OnPathChanged();
    }

    void OnPathChanged()
    {
        DistanceTravelled = CurrentPath.path.GetClosestDistanceAlongPath(transform.position);
    }
}