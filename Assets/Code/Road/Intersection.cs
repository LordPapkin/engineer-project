using System.Collections.Generic;
using UnityEngine;
using PathCreation;

public class Intersection : MonoBehaviour
{
    [field: SerializeField] public PathCreator ChosenPath { get; set; }

    [field: SerializeField] public List<PathCreator> AvaiablePaths { get; private set; }


    void OnTriggerEnter(Collider other)
    {
        SetFollowersIntersection(other.GetComponent<PathFollower>());
    }

    void SetFollowersIntersection(PathFollower follower)
    {
        if (follower == null)
            return;

        follower.EnterIntersection(this);
    }
}
