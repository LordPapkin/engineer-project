using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IntersectionUI : MonoBehaviour
{
    public List<IntersectionArrow> arrows;

    [SerializeField] private IntersectionArrow arrowPrefab;
    [SerializeField] private Canvas canvas;
    [SerializeField] private float arrowOffset = 0.5f;

    private Intersection intersection;

    private void Start()
    {
        canvas.worldCamera = Camera.main;
        intersection = GetComponent<Intersection>();

        foreach (var path in intersection.AvaiablePaths)
        {
            if (path == null)
                continue;

            var dist = path.path.GetClosestDistanceAlongPath(transform.position) + arrowOffset;
            var pos = path.path.GetPointAtDistance(dist, PathCreation.EndOfPathInstruction.Stop);
            var rot = path.path.GetRotationAtDistance(dist, PathCreation.EndOfPathInstruction.Stop);
            rot *= Quaternion.AngleAxis(90, Vector3.down) * Quaternion.AngleAxis(180, Vector3.right);

            var arrow = Instantiate(arrowPrefab, pos, rot, canvas.transform);
            var arrowPos = arrow.transform.localPosition;
            arrow.transform.localPosition = new Vector3(arrowPos.x, arrowPos.y, 0);

            arrow.Intersection = intersection;
            arrow.IntersectionUI = this;
            arrow.Path = path;

            if (intersection.ChosenPath == path)
            {
                arrow.Select();
            }

            arrows.Add(arrow);
        }
    }
}
