using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlimeFacialAnimator : MonoBehaviour
{
    [SerializeField]private Expressions TargetExpression;
    [SerializeField]private float speedMultiplier = 1;
    [SerializeField]private SkinnedMeshRenderer slimeMeshRenderer;

    private Vector2 startingShapes;
    private float currentLerp;
    private bool isInProgress;

    [SerializeField] bool debugSmile = false;
    [SerializeField] bool debugAngy = false;
    [SerializeField] bool debugHurt = false;
    private void Update()
    {
        if (debugSmile)
        {
            debugSmile = false;
            Smile();
        }

        if (debugAngy)
        {
            debugAngy = false;
            Angry();
        }

        if (debugHurt)
        {
            debugHurt = false;
            Hurt();
        }
    }

    public void Angry()
    {
        TargetExpression = Expressions.angry;
        ChangeFace();
    }

    public void Smile()
    {
        TargetExpression = Expressions.smile;
        ChangeFace();
    }

    public void Hurt()
    {
        TargetExpression = Expressions.hurt;
        ChangeFace();
    }

    private enum Expressions
    {
        smile, // angry 0, hurt 0
        angry, // angry 1, hurt 0
        hurt   // angry 0, hurt 1
    }

    public void ChangeFace()
    {
        if (isInProgress)
            return;

        currentLerp = 0;
        startingShapes = new Vector2(slimeMeshRenderer.GetBlendShapeWeight(0), slimeMeshRenderer.GetBlendShapeWeight(1));
        Vector2 targetShapes;

        switch(TargetExpression)
        {
            default:
            case Expressions.smile:
                targetShapes = Vector2.zero;
                break;
            case Expressions.angry:
                targetShapes = Vector2.right;
                break;
            case Expressions.hurt:
                targetShapes = Vector2.up;
                break;
        }

        if (targetShapes == startingShapes)
            return;

        isInProgress = true;
        StartCoroutine(BlendShapeLerp(targetShapes * 100));
    }

    private IEnumerator BlendShapeLerp(Vector2 targetShapes)
    {
        Vector2 currShape;
        do
        {
            currentLerp = Mathf.Clamp(currentLerp += Time.deltaTime * speedMultiplier, 0, 1);
            currShape = Vector2.Lerp(startingShapes, targetShapes, currentLerp);
            slimeMeshRenderer.SetBlendShapeWeight(0, currShape.x);
            slimeMeshRenderer.SetBlendShapeWeight(1, currShape.y);
            yield return null;
        }
        while ((currShape != targetShapes));
        isInProgress = false;
    }
}
