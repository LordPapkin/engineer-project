using UnityEngine;

public class LevelCameraController : MonoBehaviour
{
    [Header("References")] 
    [SerializeField] private Transform virtualCameraTransform;
    [SerializeField] private InputReader inputReader;
    [Header("Camera Settings")]
    [SerializeField] private bool mouseControl;
    [SerializeField] private float cameraMoveSpeed;
    [SerializeField] private float edgeScrollingMargin = 100f;

    private Vector2 mousePosition;
    private Vector3 moveDir;
    private Vector3 targetTransformPosition;
    
    private void Update()
    {
        HandleCameraMovement();
    }

    private void HandleCameraMovement()
    {
        ReadMoveDirFromInput();
        
        if (mouseControl)
        {
            OverrideMoveDirWithMouseInput();
        }

        var dir = Quaternion.AngleAxis(virtualCameraTransform.eulerAngles.y, Vector3.up) * moveDir;
        virtualCameraTransform.position += dir * (cameraMoveSpeed * Time.deltaTime);
    }

    private void OverrideMoveDirWithMouseInput()
    {
        ReadMousePosition();
        OverrideXAxis();
        OverrideZAxis();
        moveDir.Normalize();
    }

    private void OverrideXAxis()
    {
        if (mousePosition.x > Screen.width - edgeScrollingMargin)
        {
            moveDir.x = +1f;
        }

        if (mousePosition.x < edgeScrollingMargin)
        {
            moveDir.x = -1f;
        }
    }

    private void OverrideZAxis()
    {
        if (mousePosition.y > Screen.height - edgeScrollingMargin)
        {
            moveDir.z = +1f;
        }

        if (mousePosition.y < edgeScrollingMargin)
        {
            moveDir.z = -1f;
        }
    }

    private void ReadMoveDirFromInput()
    {
        moveDir.x = inputReader.GetMoveVector().x;
        moveDir.z = inputReader.GetMoveVector().y;
    }

    private void ReadMousePosition()
    {
        mousePosition = inputReader.MousePosition();
    }
}
